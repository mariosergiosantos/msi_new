<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*$factory->define(App\Entities\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});*/

$factory->define(App\Entities\Produtos::class, function ($faker) {
    return [
        'descricao' => $faker->sentence,
        'unidade' => $faker->word,
        'precoCompra' => rand(1, 900),
        'precoVenda' => rand(1, 1200),
        'estoque' => rand(1, 90),
        'estoqueMinimo' => rand(1, 100),
    ];
});