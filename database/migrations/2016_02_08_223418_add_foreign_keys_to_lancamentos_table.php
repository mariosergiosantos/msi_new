<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLancamentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lancamentos', function(Blueprint $table)
		{
			$table->foreign('clientes_id', 'fk_lancamentos_clientes1')->references('idClientes')->on('clientes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lancamentos', function(Blueprint $table)
		{
			$table->dropForeign('fk_lancamentos_clientes1');
		});
	}

}
