<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLancamentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lancamentos', function(Blueprint $table)
		{
			$table->integer('idLancamentos', true);
			$table->string('descricao')->nullable();
			$table->string('valor', 15);
			$table->date('data_vencimento');
			$table->date('data_pagamento')->nullable();
			$table->boolean('baixado')->nullable();
			$table->string('cliente_fornecedor')->nullable();
			$table->string('forma_pgto', 100)->nullable();
			$table->string('tipo', 45)->nullable();
			$table->string('anexo', 250)->nullable();
			$table->integer('clientes_id')->nullable()->index('fk_lancamentos_clientes1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lancamentos');
	}

}
