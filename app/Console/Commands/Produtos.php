<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ProdutosRepository as Produto;

class Produtos extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'produtos:estoqueBaixo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispara um email informando os produtos que estão em baixa';

    /**
     * Entidade produto
     * @var type 
     */
    private $produto;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Produto $produto) {
        parent::__construct();
        $this->produto = $produto;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $produtos = $this->produto->estoque();
        if (count($produtos) > 0) {
            dispatch(new \App\Jobs\SendEmailProdutosBaixo($this->formatMessage($produtos), count($produtos)));
        }
    }

    private function formatMessage($produtos) {
        $msg = "";
        foreach ($produtos as $produto) {
            $msg .= "<li class='list-group-item'>" . $produto->descricao . " - Quantidade no Estoque <span class='red'>" . $produto->estoque . "</span></li><br>";
        }
        return $msg;
    }

}
