<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        $this->loadViewsFrom(base_path('resources/views/angle'), 'angle');

        $this->publishes([
            base_path('resources/views/angle') => base_path('resources/views/vendor/angle'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        /* $this->loadViewsFrom(base_path('resources/views/pages'), 'page');
          $this->loadViewsFrom(base_path('resources/views/layouts'), 'layouts');
          $this->loadViewsFrom(base_path('resources/views/fragmentos'), 'fragmentos');
          $this->loadViewsFrom(base_path('resources/views/auth'), 'auth'); */
    }

}
