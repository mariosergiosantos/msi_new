<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(
                \App\Repositories\AnexosRepository::class, \App\Repositories\AnexosRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ClientesRepository::class, \App\Repositories\ClientesRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\DocumentosRepository::class, \App\Repositories\DocumentosRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\EmitenteRepository::class, \App\Repositories\EmitenteRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ItensDeVendasRepository::class, \App\Repositories\ItensDeVendasRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\LancamentosRepository::class, \App\Repositories\LancamentosRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\OsRepository::class, \App\Repositories\OsRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\PermissoesRepository::class, \App\Repositories\PermissoesRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ProdutosRepository::class, \App\Repositories\ProdutosRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ProdutosOsRepository::class, \App\Repositories\ProdutosOsRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ServicosRepository::class, \App\Repositories\ServicosRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ServicosOsRepository::class, \App\Repositories\ServicosOsRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\VendasRepository::class, \App\Repositories\VendasRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\PermissionRepository::class, \App\Repositories\PermissionRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\FornecedoresRepository::class, \App\Repositories\FornecedoresRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\CategoriasRepository::class, \App\Repositories\CategoriasRepositoryEloquent::class
        );
        
         $this->app->bind(
         \App\Repositories\EnderecosRepository::class, \App\Repositories\EnderecosRepositoryEloquent::class
        );
    }

}
