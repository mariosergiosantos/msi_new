<?php

namespace App\Exceptions;

use App\Jobs\SendEmailErrorApp;
use App\Jobs\SendReminderEmail;
use Exception;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Bus\DispatchesJobs;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($this->isHttpException($e)) {
            if ($e instanceof NotFoundHttpException | $e instanceof MethodNotAllowedHttpException) {
                return response()->view('angle::error.404', [], 404);
            }
            return $this->renderHttpException($e);
        }/* else {
            if ($e instanceof \ErrorException) {
                return response()->view('errors.error', [], 500);
            }
        }*/
        return parent::render($request, $e);
    }

}
