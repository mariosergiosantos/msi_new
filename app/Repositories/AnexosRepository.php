<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AnexosRepository
 * @package namespace App\Repositories;
 */
interface AnexosRepository extends RepositoryInterface
{
    //
}
