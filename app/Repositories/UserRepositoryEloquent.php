<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\User;

/**
 * Description of UserRepositoryEloquent
 *
 * @author mario
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository {
    
    private $usuario;
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome' => 'like',
        'email' => 'like'
    ];

    public function boot() {
        $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }

    public function model(){
        $this->usuario = new User();
        return User::class;
    }
    
    public function autoComplete($q) {
        return $this->usuario
                ->where('nome', 'like', '%' . $q . '%')
                ->orWhere('idUsuarios','like' , '%' . $q)
                ->get();
    }
}
