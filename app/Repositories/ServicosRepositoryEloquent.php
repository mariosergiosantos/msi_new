<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Criteria\CriteriaForName;
use App\Entities\Servicos;

/**
 * Class ServicosRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ServicosRepositoryEloquent extends BaseRepository implements ServicosRepository {

    private $servicos;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        $this->servicos = new Servicos();
        return Servicos::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(CriteriaForName::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function autoComplete($q) {
        return $this->servicos->where('nome', 'like', '%' . $q . '%')->orWhere('descricao', 'like', '%' . $q . '%')->get();
    }

}
