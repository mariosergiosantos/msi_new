<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FornecedoresRepository
 * @package namespace App\Repositories;
 */
interface FornecedoresRepository extends RepositoryInterface {

    public function autoComplete($q);
}
