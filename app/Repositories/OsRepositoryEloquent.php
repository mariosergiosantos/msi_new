<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Os;

/**
 * Class OsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OsRepositoryEloquent extends BaseRepository implements OsRepository
{

    private $ordemServico;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        $this->ordemServico = new Os();
        return Os::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function adicionarProduto($data)
    {
        $ordemServico = $this->find($data['os_id']);
        $ordemServico->produtos()->create($data);
    }

}
