<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DocumentosRepository
 * @package namespace App\Repositories;
 */
interface DocumentosRepository extends RepositoryInterface
{
    //
}
