<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Anexos;

/**
 * Class AnexosRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AnexosRepositoryEloquent extends BaseRepository implements AnexosRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Anexos::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
