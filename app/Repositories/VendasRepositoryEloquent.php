<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Criteria\CriteriaForDateUpdate;
use App\Entities\Vendas;

/**
 * Class VendasRepositoryEloquent
 * @package namespace App\Repositories;
 */
class VendasRepositoryEloquent extends BaseRepository implements VendasRepository {
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'documento' => 'like',
        'descricao' => 'like',
        'extensao' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Vendas::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(CriteriaForDateUpdate::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
