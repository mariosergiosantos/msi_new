<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissoesRepository
 * @package namespace App\Repositories;
 */
interface PermissoesRepository extends RepositoryInterface
{
    //
}
