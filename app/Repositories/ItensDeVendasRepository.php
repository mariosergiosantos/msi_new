<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ItensDeVendasRepository
 * @package namespace App\Repositories;
 */
interface ItensDeVendasRepository extends RepositoryInterface
{
    //
}
