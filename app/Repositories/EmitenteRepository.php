<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmitenteRepository
 * @package namespace App\Repositories;
 */
interface EmitenteRepository extends RepositoryInterface
{
    //
}
