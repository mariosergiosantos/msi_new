<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\ItensDeVendas;

/**
 * Class ItensDeVendasRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ItensDeVendasRepositoryEloquent extends BaseRepository implements ItensDeVendasRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ItensDeVendas::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria( app(RequestCriteria::class) );
    }
}