<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Criteria\CriteriaForName;
use App\Entities\Produtos;

/**
 * Class ProdutosRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProdutosRepositoryEloquent extends BaseRepository implements ProdutosRepository {

    private $produtos;
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        $this->produtos = new Produtos();
        return Produtos::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(CriteriaForName::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * 
     * @param type $q
     * @return type
     */
    public function autoComplete($q) {
        return $this->produtos
                ->where('nome', 'like', '%' . $q . '%')
                ->where('status', '=', '1')
                ->orWhere('idProdutos','like' , '%' . $q)
                ->get();
    }

    /**
     * Realiza a busca por produtos com estoque baixo
     * @return type
     */
    public function estoque() {
        return \DB::select(\DB::raw('SELECT * FROM `produtos` where estoque <= estoqueMinimo'));
    }

}
