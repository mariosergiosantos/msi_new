<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Fornecedores;

/**
 * Class FornecedoresRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FornecedoresRepositoryEloquent extends BaseRepository implements ClientesRepository {

    private $fornecedor;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        $this->fornecedor = new Fornecedores();
        return Fornecedores::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function autoComplete($q) {
        return $this->fornecedor->where('nome', 'like', '%' . $q . '%')->get();
    }

}
