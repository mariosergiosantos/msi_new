<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Lancamentos;

/**
 * Class LancamentosRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LancamentosRepositoryEloquent extends BaseRepository implements LancamentosRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Lancamentos::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
