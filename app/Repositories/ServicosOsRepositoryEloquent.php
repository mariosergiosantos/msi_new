<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\ServicosOs;

/**
 * Class ServicosOsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ServicosOsRepositoryEloquent extends BaseRepository implements ServicosOsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ServicosOs::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria( app(RequestCriteria::class) );
    }
}