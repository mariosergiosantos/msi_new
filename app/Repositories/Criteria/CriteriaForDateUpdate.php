<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Description of CriteriaForDateUpdate
 *
 * @author mario
 */
class CriteriaForDateUpdate implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository) {
        $model = $model->orderBy('updated_at', 'desc');
        return $model;
    }

}
