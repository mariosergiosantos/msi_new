<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProdutosOsRepository
 * @package namespace App\Repositories;
 */
interface ProdutosOsRepository extends RepositoryInterface
{
    //
}
