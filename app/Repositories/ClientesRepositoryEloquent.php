<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Criteria\CriteriaForName;
use App\Entities\Clientes;

/**
 * Class ClientesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ClientesRepositoryEloquent extends BaseRepository implements ClientesRepository {

    private $clientes;
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome' => 'like',
        'email' => 'like',
        'idClientes' => 'like',
        'bairro' => 'like',
        'cidade' => 'like',
        'estado' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        $this->clientes = new Clientes();
        return Clientes::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(CriteriaForName::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function autoComplete($q) {
        return $this->clientes->where('nome', 'like', '%' . $q . '%')->get();
    }

}
