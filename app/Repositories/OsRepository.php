<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OsRepository
 * @package namespace App\Repositories;
 */
interface OsRepository extends RepositoryInterface
{
    public function adicionarProduto($data);
}
