<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServicosOsRepository
 * @package namespace App\Repositories;
 */
interface ServicosOsRepository extends RepositoryInterface
{
    //
}
