<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Documentos;

/**
 * Class DocumentosRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DocumentosRepositoryEloquent extends BaseRepository implements DocumentosRepository {
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'documento' => 'like',
        'descricao' => 'like',
        'extensao' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Documentos::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
