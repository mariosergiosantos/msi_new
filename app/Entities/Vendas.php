<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Vendas extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "vendas";
    protected $primaryKey = 'idVendas';
    protected $fillable = [
        'descricao',
        'valor_total',
        'desconto',
        'frete',
        'outros',
        'quantidade_itens',
        'faturado',
        'forma_pagamento_id',
        'parcelamento',
        'data_vencimento',
        'data_pagamento',
        'clientes_id',
        'usuarios_id',
        'lancamentos_id'
    ];

    public function cliente() {
        return $this->belongsTo(Clientes::class, 'clientes_id');
    }

    public function usuario() {
        return $this->belongsTo(User::class, 'usuarios_id');
    }

    public function lancamentos() {
        return $this->belongsTo(Lancamentos::class, 'lancamentos_id');
    }
    
    public function itens(){
    	return $this->hasMany(ItensDeVendas::class);
    }

}
