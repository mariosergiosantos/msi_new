<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Marca extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "marcas";
    protected $fillable = [
        'nome'
    ];

}
