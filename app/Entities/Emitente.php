<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Emitente extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = "emitente";
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'nome',
        'cnpj',
        'ie',
        'rua',
        'numero',
        'bairro',
        'cidade',
        'uf',
        'telefone',
        'email',
        'url_logo'
    ];

}
