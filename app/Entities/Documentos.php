<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Documentos extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "documentos";
    protected $primaryKey = 'idDocumentos';
    public $timestamps = false;
    protected $fillable = [
        'documento',
        'descricao',
        'file',
        'path',
        'url',
        'extensao',
        'tamanho'
    ];

}
