<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Categorias extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "categorias";
    protected $fillable = [
        'nome'
    ];

    public function produtos() {
        return $this->hasMany(Produtos::class);
    }

}
