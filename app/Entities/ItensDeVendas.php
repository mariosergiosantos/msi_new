<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ItensDeVendas extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "itens_de_vendas";
    protected $primaryKey = 'idItens';
    public $timestamps = false;
    protected $fillable = [
        'subTotal',
        'quantidade',
        'vendas_id',
        'produtos_id'
    ];

    public function vendas() {
        return $this->belongsTo(Vendas::class);
    }

    public function produtos() {
        return $this->belongsTo(Produtos::class);
    }

}
