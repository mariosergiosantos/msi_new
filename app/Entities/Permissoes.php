<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Permissoes extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "permissoes";
    protected $primaryKey = 'idPermissoes';
    public $timestamps = false;
    protected $fillable = [
    ];

}
