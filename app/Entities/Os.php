<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Os extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "os";
    public $timestamps = false;
    protected $fillable = [
        'dataInicial',
        'dataFinal',
        'garantia',
        'descricaoProduto',
        'defeito',
        'status',
        'observacoes',
        'laudoTecnico',
        'valorTotal',
        'clientes_id',
        'usuarios_id',
        'lancamento',
        'faturado'
    ];
    protected $primaryKey = 'idOs';
    
    public function cliente() {
        return $this->belongsTo(Clientes::class, 'clientes_id');
    }

    public function usuario() {
        return $this->belongsTo(User::class, 'usuarios_id');
    }

    public function lancamento() {
        return $this->belongsTo(Lancamentos::class, 'lancamento');
    }

    public function produtos() {
        return $this->hasMany(ProdutosOs::class);
    }

    public function servicos() {
        return $this->hasMany(ServicosOs::class);
    }

    public function anexos() {
        return $this->hasMany(Anexos::class);
    }

}
