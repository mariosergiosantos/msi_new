<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Clientes extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "clientes";
    protected $fillable = [
        'nome',
        'documento',
        'telefone',
        'celular',
        'email'
    ];
    protected $primaryKey = 'idClientes';

    public function os() {
        return $this->hasMany(Os::class);
    }

    public function lancamentos() {
        return $this->hasMany(Lancamentos::class);
    }

    public function vendas() {
        return $this->hasMany(Vendas::class);
    }

    public function itens() {
        return $this->hasManyThrough(ItensDeVendas::class, Vendas::class);
    }
    
    public function endereco(){
        return $this->hasMany(Enderecos::class);
    }

}
