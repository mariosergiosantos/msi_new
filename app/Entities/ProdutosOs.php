<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProdutosOs extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "produtos_os";
    protected $primaryKey = 'idProdutos_os';
    public $timestamps = false;

    protected $fillable = [
        'quantidade',
        'os_id',
        'produtos_id',
        'subTotal'
    ];
    
    public function produto(){
        return $this->belongsTo(Produtos::class, 'produtos_id');
    }

}
