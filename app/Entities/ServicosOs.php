<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ServicosOs extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "servicos_os";
    protected $primaryKey = 'idServicos_os';
    public $timestamps = false;
    protected $fillable = [
        'os_id',
        'servicos_id',
        'subTotal'
    ];

    public function servico() {
        return $this->belongsTo(Servicos::class, 'servicos_id');
    }
    
    public function os(){
        return $this->belongsTo(Os::class, 'os_id');
    }

}
