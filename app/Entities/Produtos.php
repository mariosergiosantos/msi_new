<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Produtos extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "produtos";
    protected $primaryKey = 'idProdutos';
    protected $fillable = [
        'nome',
        'categoria_id',
        'fornecedor_id',
        'marca_id',
        'preco_compra',
        'preco_venda',
        'estoque',
        'estoque_minimo'
    ];
    
    public function itens(){
    	return $this->hasMany(ItensDeVendas::class);
    }
    
    public function categoria(){
        return $this->belongsTo(Categorias::class);
    }
    
    public function fornecedor(){
        return $this->belongsTo(Fornecedores::class);
    }
    
    public function marca(){
        return $this->belongsTo(Marca::class);
    }

}
