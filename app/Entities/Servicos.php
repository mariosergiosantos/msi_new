<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Servicos extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = "servicos";
    protected $primaryKey = 'idServicos';
    protected $fillable = [
        'nome',
        'descricao',
        'preco'
    ];

}
