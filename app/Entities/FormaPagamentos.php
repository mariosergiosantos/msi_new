<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class FormaPagamentos extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "forma_pagamentos";
    protected $fillable = [
        'nome'
    ];

}
