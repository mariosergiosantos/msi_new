<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Lancamentos extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "lancamentos";
    protected $primaryKey = 'idLancamentos';
    public $timestamps = false;
    protected $fillable = [
        'descricao',
        'valor',
        'data_vencimento',
        'data_pagamento',
        'recebido',
        'cliente_fornecedor',
        'forma_pgto',
        'tipo',
        'anexo',
        'clientes_id'
    ];

    public function clientes() {
        return $this->belongsTo(User::class);
    }

}
