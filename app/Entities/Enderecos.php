<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Enderecos extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "enderecos";
    protected $fillable = [
        'cliente_id',
        'estado',
        'cidade',
        'bairro',
        'cep',
        'endereco',
        'rua',
        'numero',
        'complemento'
    ];
    
    public function cliente(){
        return $this->belongsTo(Clientes::class);
    }
    
}
