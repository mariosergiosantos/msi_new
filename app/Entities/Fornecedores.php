<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Fornecedores extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "fornecedores";
    public $timestamps = false;
    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'documento',
        'estado',
        'cidade',
        'cep'
    ];

    public function produtos() {
        return $this->hasMany(Produtos::class);
    }

}
