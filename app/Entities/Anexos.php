<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Anexos extends Model implements Transformable {

    use TransformableTrait;

    protected $table = "anexos";
    protected $primaryKey = 'idAnexos';
    public $timestamps = false;
    protected $fillable = [
        'anexo',
        'thumb',
        'url',
        'path',
        'os_id'
    ];
    
    public function os(){
        return $this->hasOne(Os::class, 'os_id');
    }

}
