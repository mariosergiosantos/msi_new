<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\VendasRepository as Repository;
use App\Validator\VendaValidator as Validator;
use App\Services\Services;

/**
 * Description of VendaServices
 *
 * @author mario
 */
class VendaServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     * 
     * @param Repository $repository
     * @param Validator $validator
     */
    public function __construct(Repository $repository, Validator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.sale');
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->with(['itens', 'usuario', 'cliente'])->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

}
