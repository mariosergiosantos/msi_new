<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\OsRepository;
use App\Validator\OsValidator;

//use App\Services\Services;

/**
 * Description of OsServices
 *
 * @author mario
 */
class OsServices
{

    use Services;

    /**
     *
     * @var type
     */
    protected $repository;

    /**
     *
     * @var type
     */
    protected $validator;

    /**
     *
     * @var type
     */
    protected $nofound;

    /**
     *
     * @param ClienteRepository $repository
     * @param ClienteValidator $validator
     */
    public function __construct(OsRepository $repository, OsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.client');
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function show($id)
    {
        try {
            return $this->repository->with(['cliente', 'usuario', 'lancamento', 'produtos', 'servicos', 'anexos'])->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound . " [DEV] " . $e->getMessage()
            ];
        }
    }

    /**
     * @param $request
     */
    public function adicionarProduto($request)
    {
        $data = [
            'quantidade' => $request['quantidade'],
            'os_id' => (int)$request['idOsProduto'],
            'produtos_id' => $request['idProduto'],
            'subTotal' => $request['quantidade'] * $request['preco']
        ];
        $this->repository->adicionarProduto($data);
    }


    public function adicionarServico($request)
    {

    }

}
