<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ProdutosRepository;
use App\Validator\ProdutosValidator;
use App\Services\Services;

/**
 * Description of ProdutosServices
 *
 * @author mario
 */
class ProdutosServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;
    
    /**
     *
     * @var type 
     */
    protected $entity;

    /**
     * 
     * @param ClienteRepository $repository
     * @param ClienteValidator $validator
     */
    public function __construct(ProdutosRepository $repository, ProdutosValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.product');
        $this->entity = trans('messages.page.product');
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->with(['itens'])->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }
    
    /**
     * 
     * @param type $q
     * @return type
     */
    public function autoComplete($q) {
        $produtos = $this->repository->autoComplete($q);
        $data = array();
        foreach ($produtos as $produto) {
            $data[] = array(
                'label' => $produto->nome . ' | Preço: ' . $produto->preco_venda . ' | Estoque: ' . $produto->estoque,
                'id' => $produto->idProdutos,
                'preco_venda' => $produto->preco_venda,
                'estoque' => $produto->estoque
            );
        }
        return response()->json($data);
    }

}
