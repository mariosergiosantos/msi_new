<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\DocumentosRepository as Repository;
use App\Validator\DocumentoValidator as Validator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Filesystem\Filesystem;
use App\Services\Services;

/**
 * Description of DocumentoServices
 *
 * @author mario
 */
class DocumentoServices {

    use Services;
    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     *
     * @var type 
     */
    private $filesystem;

    /**
     * 
     * @param ClienteRepository $repository
     * @param ClienteValidator $validator
     */
    public function __construct(Repository $repository, Validator $validator, Filesystem $file) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->filesystem = $file;
        $this->nofound = trans('messages.nofound.client');
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        try {
            $this->repository->delete($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

}
