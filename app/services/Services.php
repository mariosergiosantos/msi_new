<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;
use Prettus\Validator\Exceptions\ValidatorException;
/**
 * Description of ServicesAbstract
 *
 * @author mario
 */
trait Services {

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        try {
            $this->repository->delete($id);
        } catch (\Exception $exception) {
            return [
                'error' => 'true',
                'messege' => $this->processException($exception)
            ];
        }
    }
    
    private function processException($exception){
        if($exception->getCode() == 23000){
            return "Este " . $this->entity . " não pode ser excluido por ter relação com alguma ordem de seviço e/ou venda";
        } 
        
        return $this->nofound;
    }

}
