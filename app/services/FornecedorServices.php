<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\FornecedoresRepositoryEloquent as Repository;
use App\Validator\FornecedorValidator as Validator;
use App\Services\Services;

/**
 * Description of FornecedorServices
 *
 * @author mario
 */
class FornecedorServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     * 
     * @param Repository $repository
     * @param Validator $validator
     */
    public function __construct(Repository $repository, Validator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.provider');
    }

    /**
     * 
     * @param type $q
     * @return type
     */
    public function autoComplete($q) {
        $clientes = $this->repository->autoComplete($q);
        $data = array();
        foreach ($clientes as $cliente) {
            $data[] = array(
                'label' => $cliente->nomeCliente . ' | Telefone: ' . $cliente->telefone,
                'id' => $cliente->idClientes
            );
        }
        return response()->json($data);
    }

}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               