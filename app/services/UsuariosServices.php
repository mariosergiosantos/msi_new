<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\UserRepository as Repository;
use App\Validator\UsuarioValidator as Validator;
use App\Services\Services;

/**
 * Description of UsuariosServices
 *
 * @author mario
 */
class UsuariosServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     * 
     */
    public function __construct(Repository $repository, Validator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.user');
    }

    /**
     * 
     * @param type $q
     * @return type
     */
    public function autoComplete($q) {
        $usuarios = $this->repository->autoComplete($q);
        $data = array();
        foreach ($usuarios as $usuario) {
            $data[] = array(
                'label' => $usuario->nome . ' | Telefone: ' . $usuario->telefone,
                'id' => $usuario->idUsuarios
            );
        }
        return response()->json($data);
    }

}
