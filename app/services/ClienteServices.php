<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ClientesRepository;
use App\Validator\ClienteValidator;
use App\Services\Services;

/**
 * Description of ClienteServices
 *
 * @author mario
 */
class ClienteServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     * 
     * @param ClienteRepository $repository
     * @param ClienteValidator $validator
     */
    public function __construct(ClientesRepository $repository, ClienteValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.client');
    }

    public function autoComplete($q) {
        $clientes = $this->repository->autoComplete($q);
        $data = array();
        foreach ($clientes as $cliente) {
            $data[] = array(
                'label' => $cliente->nome . ' | Telefone: ' . $cliente->telefone,
                'id' => $cliente->idClientes
            );
        }
        return response()->json($data);
    }

}
