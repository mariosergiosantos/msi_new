<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ItensDeVendasRepository as Repository;
use App\Services\Services;
use App\Validator\ItensVendaValidator as Validator;

/**
 * Description of ItensDeVendaServices
 *
 * @author mario
 */
class ItensDeVendaServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;
    
    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     * 
     * @param Repository $repository
     */
    public function __construct(Repository $repository, Validator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.product');
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->with(['produtos'])->findWhere(['vendas_id' => $id]);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessage()
            ];
        }
    }

}
