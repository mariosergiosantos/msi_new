<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ServicosRepository as Repository;
use App\Validator\ServicoValidator as Validator;
use App\Services\Services;

/**
 * Description of ServicoServices
 *
 * @author mario
 */
class ServicoServices {

    use Services;

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     *
     * @var type 
     */
    protected $nofound;

    /**
     * 
     * @param ClienteRepository $repository
     * @param ClienteValidator $validator
     */
    public function __construct(Repository $repository, Validator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.client');
    }

    public function autoComplete($q) {
        $servicos = $this->repository->autoComplete($q);
        $data = array();
        foreach ($servicos as $servico) {
            $data[] = array(
                'label' => $servico->nome . ' | Preço: ' . $servico->preco,
                'id' => $servico->idServicos,
                'preco' => $servico->preco
            );
        }
        return response()->json($data);
    }

}
