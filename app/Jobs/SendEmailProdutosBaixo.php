<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;
use Illuminate\Http\Request;

class SendEmailProdutosBaixo extends Job implements SelfHandling, ShouldQueue {

    use InteractsWithQueue,
        SerializesModels;

    private $produtos;
    
    private $qtd;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($produtos, $qtd) {
        $this->produtos = $produtos;
        $this->qtd = $qtd;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer) {
        $mailer->queue('emails.estoque-baixo', ['produtos' => $this->produtos, 'qtd' => $this->qtd], function ($x) {
            $x->from('mariodesenvolvedor@gmail.com', 'MSI');
            $x->to('mariosergio952@hotmail.com.br', "Mário Sérgio")->subject('Alerta sobre o estoque dos produtos');
        });
    }

}
