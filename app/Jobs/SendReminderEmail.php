<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;

class SendReminderEmail extends Job implements SelfHandling, ShouldQueue {

    use InteractsWithQueue,
        SerializesModels;

    private $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($msg) {
        $this->message = $msg;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer) {
        $mailer->send('emails.teste', ['user' => $this->message], function ($x) {
            $x->from('mariodesenvolvedor@gmail.com', 'InfoInvest');
            $x->to('mariosergio952@hotmail.com.br', "Mário Sérgio")->subject('Novas Notícias!');
        });
    }

}
