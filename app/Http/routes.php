<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return Redirect::to('auth/login');
    //return view('angle.dashboard');
});

Route::group(['middleware' => 'auth'], function() {
    
    Route::post('vendas/forma-pagamento', 'VendasController@postFormaPagamento');
    Route::get('vendas/orcamento', 'VendasController@getOrcamento');
    
    Route::get('compras/orcamento', 'VendasController@opcoesParcelamento');
    
    Route::get('dashboard', 'DashboardController@index');
    Route::resource('arquivos', 'ArquivosController');
    Route::resource('clientes', 'ClientesController');
    Route::resource('fornecedores', 'FornecedorController');
    Route::resource('produtos', 'ProdutosController');
    //Route::resource('servicos', 'ServicosController');
    //Route::resource('os', 'OrdemServicoController');
    Route::resource('vendas', 'VendasController');
    Route::resource('lancamentos', 'FinanceiroController');
    Route::resource('usuarios', 'UsuariosController');
    Route::resource('emitente', 'EmitentesController');
    Route::resource('permissoes', 'PermissoesController');
    Route::controllers([
        'relatorios' => 'RelatoriosController'
    ]);
    
    Route::get('search', 'SearchController@search');

    /**
     * Autocomplete
     */
    Route::get('autocomplete/cliente', 'ClientesController@autoComplete');
    Route::get('autocomplete/usuario', 'UsuariosController@autoComplete');
    Route::get('autocomplete/produto', 'ProdutosController@autoComplete');
    //Route::get('autocomplete/servico', 'ServicosController@autoComplete');

    /*Route::post('os/produto', 'OrdemServicoController@postProduto');
    Route::delete('os/produto/{id}', 'OrdemServicoController@deleteProduto');
    
    Route::post('os/servico', 'OrdemServicoController@postServico');
    Route::delete('os/servico/{id}', 'OrdemServicoController@deleteServico');
    
    Route::post('os/anexo', 'OrdemServicoController@postAnexo');
    Route::delete('os/anexo/{id}', 'OrdemServicoController@deleteAnexo');*/
    
    Route::post('vendas/produto', 'VendasController@postProduto');
    Route::delete('vendas/produto/{id}', 'VendasController@deleteProduto');
    
    Route::get('vendas/opcoesParcelamento/{id}', 'VendasController@opcoesParcelamento');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Event::listen('illuminate.query', function ($sql) {
    //print_R($sql);
});
