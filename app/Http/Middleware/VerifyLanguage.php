<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Services\ConfiguracaoServices as Service;

class VerifyLanguage
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    
    private $service;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, Service $service)
    {
        $this->auth = $auth;
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($request->session()->has('language')) {
            \App::setLocale($request->session()->get('language'));
        }else if(!$this->auth->guest()) {
            $conf = $this->service->show($this->auth->user()->id);
            $this->service->updateLanguage($this->service->getSiglaLanguage($conf->idioma_id));
        }
        return $next($request);
    }
}
