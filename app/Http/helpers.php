<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('qrCode')) {

    /**
     * Make a new url to QrCode from APi Google chart
     *
     * @param  String  $url
     * @param  int  $width
     * @param  int  $height
     * @return mixed
     */
    function qrCode($url, $width = 300, $height = 300) {
        return "https://chart.googleapis.com/chart?chs=" . $width . 'x' . $height . "&cht=qr&chl=" . $url;
    }

}

if (!function_exists('dateCurrent')) {

    /**
     * Retorn a new current date
     * @param type $format
     * @return type
     */
    function dateCurrent($format = "d-m-Y") {
        return date($format);
    }

}