<?php

namespace App\Http\Controllers;

use App\Services\ServicoServices as Service;
use App\Repositories\ServicosRepository as Repository;

class ServicosController extends Controller {

    use TraitController;

    protected $url = "servicos";

    /**
     *
     * @var type 
     */
    protected $folderView = "servicos.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "service";

    /**
     * 
     * @param Repository $servico
     * @param Service $service
     */
    public function __construct(Repository $servico, Service $service) {
        $this->repository = $servico;
        $this->service = $service;
    }

}
