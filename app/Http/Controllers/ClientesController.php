<?php

namespace App\Http\Controllers;

use App\Services\ClienteServices as Service;
use App\Repositories\ClientesRepository as Repository;
use App\Services\ItensDeVendaServices;
use App\Repositories\EnderecosRepository;
use Illuminate\Http\Request;

class ClientesController extends Controller {

    use TraitController;

    protected $url = "clientes";

    /**
     *
     * @var type 
     */
    protected $folderView = "clientes.";

    /**
     *
     * @var type 
     */
    protected $repository;
    
    /**
     *
     * @var type 
     */
    protected $endereco;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "client";

    /**
     * 
     * @param Repository $cliente
     * @param Service $service
     */
    public function __construct(Repository $cliente, Service $service,
            ItensDeVendaServices $produtosServices, EnderecosRepository $enderecosRepository) {
        $this->repository = $cliente;
        $this->service = $service;
        $this->produto = $produtosServices;
        $this->endereco = $enderecosRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $clientes = $this->repository->with(['os'])->paginate(10);
        return view($this->folderView . 'index', compact('clientes', 'request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        $data['endereco_id'] = $this->createEndereco($request);
        $result = $this->service->create($request->all());
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.' . $this->entityLang));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function show($code) {
        $result = $this->repository->with(['vendas', 'itens', 'endereco'])->find($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }

        //dd($result);
        return view($this->folderView . 'visualizar', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code) {
        $result = $this->service->update($request->all(), $code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $cliente = $this->repository->with(['endereco'])->find($result->idClientes);
        $endereco1Cliente = $cliente->endereco[0];
        $this->updateEndereco1($request, $endereco1Cliente->id);
        return redirect($this->url)->with("success", trans('messages.update.' . $this->entityLang));
    }

    private function createEndereco($request) {
        $endereco = $this->enderecoFromRequest($request);
        return 2;
    }

    private function updateEndereco1($request, $idEndereco) {
        $this->endereco->update($this->enderecoFromRequest($request), $idEndereco);
    }

    private function enderecoFromRequest($request) {
        return $request->only([
                    'estado',
                    'cidade',
                    'bairro',
                    'cep',
                    'endereco',
                    'rua',
                    'numero',
                    'complemento'
        ]);
    }

}
