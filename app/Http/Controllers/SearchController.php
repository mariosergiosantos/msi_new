<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ClientesRepository;
use App\Repositories\FornecedoresRepositoryEloquent;
use App\Repositories\ProdutosRepository;
use App\Repositories\DocumentosRepository;

class SearchController extends Controller {

    /**
     * @var Request $request
     */
    private $request;

    /**
     *
     * @var type 
     */
    private $clientes;

    /**
     *
     * @var type 
     */
    private $fornecedores;

    /**
     *
     * @var type 
     */
    private $produtos;

    /**
     *
     * @var type 
     */
    private $arquivos;

    public function __construct(Request $request, ClientesRepository $clientes, FornecedoresRepositoryEloquent $fornecedores, ProdutosRepository $produtos, DocumentosRepository $arquivos) {
        $this->request = $request;
        $this->clientes = $clientes;
        $this->fornecedores = $fornecedores;
        $this->produtos = $produtos;
        $this->arquivos = $arquivos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search() {

        $queryLike = "%" . $this->request->get('q') . "%";

        $data['clientes'] = $this->clientes->findWhere([['nome', 'like', $queryLike]]);
        $data['fornecedores'] = $this->fornecedores->findWhere([['nome', 'like', $queryLike]]);
        $data['produtos'] = $this->produtos->findWhere([['nome', 'like', $queryLike]]);
        $data['arquivos'] = $this->arquivos->findWhere([['documento', 'like', $queryLike]]);

        return view('mapos.pesquisa', compact('data'));
    }

}
