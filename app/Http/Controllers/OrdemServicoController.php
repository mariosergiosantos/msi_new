<?php

namespace App\Http\Controllers;

use App\Services\OsServices as Service;
use App\Repositories\OsRepository as Repository;
use App\Services\ProdutosOsServices;
use App\Services\ServicosOsServices;
use App\Repositories\EmitenteRepository;
use Illuminate\Http\Request;

class OrdemServicoController extends Controller {

    use TraitController;

    protected $url = "os";

    /**
     *
     * @var type 
     */
    protected $folderView = "os.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "os";
    
    /**
     *
     * @var type 
     */
    private $produtoOs;

    /**
     * 
     * @param Repository $repository
     * @param Service $service
     */
    public function __construct(Repository $repository, Service $service, ProdutosOsServices $produtosOsServices, ServicosOsServices $servicosOsServices) {
        $this->repository = $repository;
        $this->service = $service;
        $this->produtoOs = $produtosOsServices;
        $this->serviceOs = $servicosOsServices;
    }
    
    /**
     * 
     * @param EmitenteRepository $emitenteRepository
     * @param int $code
     * @return \Illuminate\Http\Response
     */
    public function show(EmitenteRepository $emitenteRepository, $code) {
        $result = $this->service->show($code);
        $this->authorize('show-product');
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $produtos = $this->produtoOs->show($result->idOs);
        $servicos = $this->serviceOs->show($result->idOs);
        //dd($result);
        $emitente = $emitenteRepository->all();
        return view($this->folderView . 'visualizar', compact('result', 'emitente', 'produtos', 'servicos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function edit($code) {
        $result = $this->service->show($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $idOs = $result->idOs;
        $produtosOs = $this->produtoOs->show($idOs);
        $servicosOs = $this->serviceOs->show($idOs);
        return view($this->folderView . 'editar', compact('result', 'produtosOs', 'servicosOs'));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function postProduto(Request $request) {
        return $this->storeAjax($this->produtoOs, $request->all());
    }

    /**
     * 
     * @param type $idProduct
     * @return type
     */
    public function deleteProduto($idProduct) {
        return $this->deleteAjax($this->produtoOs, $idProduct);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function postServico(Request $request) {
        return $this->storeAjax($this->serviceOs, $request->all());
    }

    /**
     * 
     * @param type $servicoId
     * @return type
     */
    public function deleteServico($servicoId) {
        return $this->deleteAjax($this->serviceOs, $servicoId);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function postAnexo(Request $request) {
        return $this->storeAjax($this->produtoOs, $request->all());
    }

    /**
     * 
     * @param type $anexoID
     * @return type
     */
    public function deleteAnexo($anexoID) {
        return $this->deleteAjax($this->produtoOs, $anexoID);
    }

}
