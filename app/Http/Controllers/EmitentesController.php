<?php

namespace App\Http\Controllers;

use App\Repositories\EmitenteRepository as Repository;
use App\Services\EmitenteServices as Service;

class EmitentesController extends Controller {

    use TraitController;

    protected $url = "emitente";

    /**
     *
     * @var type 
     */
    protected $folderView = "emitentes.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "issuer";

    /**
     * 
     * @param Clientes $clientes
     */
    public function __construct(Repository $repository, Service $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

}
