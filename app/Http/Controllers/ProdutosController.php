<?php

namespace App\Http\Controllers;

use App\Services\ProdutosServices as Service;
use App\Repositories\ProdutosRepository as Repository;
use App\Repositories\FornecedoresRepositoryEloquent as FornecedoresRepository;
use App\Repositories\CategoriasRepository;
use Picqer\Barcode\BarcodeGeneratorSVG as Barcode;
use Illuminate\Http\Request;
use App\Entities\Marca;

class ProdutosController extends Controller {
    
    use TraitController;

    protected $url = "produtos";

    /**
     *
     * @var type 
     */
    protected $folderView = "produtos.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "product";

    /**
     * 
     * @param Repository $produtos
     * @param Service $service
     */
    public function __construct(Repository $produtos, Service $service) {
        $this->repository = $produtos;
        $this->service = $service;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        $results = $this->repository->with(['categoria', 'fornecedor', 'marca'])->findWhere(['status' => '1']);
        return view($this->folderView . 'index', compact('results', 'request'));
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function show(Barcode $barcode, $code) {
        $result = $this->service->show($code);
        //$this->authorize('show-product');
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return view($this->folderView . 'visualizar', compact('result', 'barcode'));
    }
    
    public function create(FornecedoresRepository $fornecedoresRepository, CategoriasRepository $categoriasRepository, Marca $marca){
        $fornecedores = $fornecedoresRepository->all();
        $categorias = $categoriasRepository->all();
        $marcas = $marca->all();
        return view($this->folderView . 'adicionar', compact('fornecedores', 'categorias', 'marcas'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function edit(FornecedoresRepository $fornecedoresRepository, CategoriasRepository $categoriasRepository, Marca $marca, $code) {
        $result = $this->service->show($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        
        $dados['fornecedores'] = $fornecedoresRepository->all();
        $dados['categorias'] = $categoriasRepository->all();
        $dados['marcas'] = $marca->all();
        return view($this->folderView . 'editar', compact('result', 'dados'));
    }
    
}
