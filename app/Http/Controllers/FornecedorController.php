<?php

namespace App\Http\Controllers;

use App\Services\FornecedorServices as Service;
use App\Repositories\FornecedoresRepositoryEloquent as Repository;

class FornecedorController extends Controller {

    use TraitController;

    protected $url = "fornecedores";

    /**
     *
     * @var type 
     */
    protected $folderView = "fornecedores.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "provider";

    /**
     * 
     * @param Repository $fornecedor
     * @param Service $service
     */
    public function __construct(Repository $fornecedor, Service $service) {
        $this->repository = $fornecedor;
        $this->service = $service;
    }

}
