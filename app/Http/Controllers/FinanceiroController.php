<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\LancamentoServices as Service;
use App\Repositories\LancamentosRepository as Repository;

class FinanceiroController extends Controller {

    use TraitController;

    protected $url = "lancamentos";

    /**
     *
     * @var type 
     */
    protected $folderView = "financeiro.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "financial";

    /**
     * 
     * @param Repository $financeiro
     * @param Service $service
     */
    public function __construct(Repository $financeiro, Service $service) {
        $this->repository = $financeiro;
        $this->service = $service;
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function index(Request $request, \Carbon\Carbon $carbon) {

        $results = $this->repository->all();

        if ($request->has('situacao')) {
            if ($request->get('situacao') == "atrasado") {
                $results = $results->where('recebido', 2);
            } else if ($request->get('situacao') == "pago") {
                $results = $results->where('recebido', 1);
            } else if ($request->get('situacao') == "pendente") {
                $results = $results->where('recebido', 0);
            }
        }

        if ($request->has('periodo')) {
            if ($request->get('periodo') == "dia") {
                $results = $results->filter(function ($item) use ($carbon) {
                    return strtotime($item->created_at) > strtotime($carbon->yesterday());
                });
            } else if ($request->get('periodo') == "semana") {
                $results = $results->filter(function ($item) use ($carbon) {
                    return strtotime($item->created_at) > strtotime($carbon->subWeek(1));
                });
            } else if ($request->get('periodo') == "mes") {
                $results = $results->filter(function ($item) use ($carbon) {
                    return strtotime($item->created_at) > strtotime($carbon->subMonths(1));
                });
            } else if ($request->get('periodo') == "ano") {
                $results = $results->filter(function ($item) use ($carbon) {
                    return strtotime($item->created_at) > strtotime($carbon->subYear(1));
                });
            }
        }

        return view($this->folderView . 'lancamentos', compact('results', 'request'));
    }

    /**
     * 
     * @param type $request
     * @return type
     */
    public function store(Request $request) {
        $data = $request->all();
        $data['clientes_id'] = $request->user()->idUsuarios;
        $lancamento = $this->service->create($data);
        if ($lancamento['error']) {
            return redirect($this->url)->with("danger", $lancamento['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.' . $this->entityLang));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code) {
        $data = $request->all();
        $data['clientes_id'] = $request->user()->idUsuarios;
        $result = $this->service->show($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $this->service->update($data, $code);
        return redirect($this->url)->with("success", trans('messages.update.' . $this->entityLang));
    }

}
