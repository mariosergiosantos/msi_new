<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ClienteServices as Service;
use App\Repositories\UserRepository;

class ConfiguracoesController extends Controller {

    protected $url = "configuracoes";
    

    public function getUsuarios(UserRepository $user) {
        return view('usuarios.index', ['results' => $user->all()]);
    }

    public function getEmitente() {
        return view($this->folderView . 'emitente');
    }

    public function getPermissoes() {
        return view($this->folderView . 'permissoes');
    }

    public function getBackup() {
        
    }
    
    public function getMinhaConta(){
        
    }

}
