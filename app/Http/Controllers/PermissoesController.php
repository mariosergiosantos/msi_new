<?php

namespace App\Http\Controllers;

use App\Services\PermissoesServices as Service;
use App\Repositories\PermissoesRepository as Repository;

class PermissoesController extends Controller {

    use TraitController;

    protected $url = "permissoes";

    /**
     *
     * @var type 
     */
    protected $folderView = "permissoes.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "permissions";

    /**
     * 
     * @param Repository $repository
     * @param Service $service
     */
    public function __construct(Repository $repository, Service $service) {
        $this->repository = $repository;
        $this->service = $service;
    }

}
