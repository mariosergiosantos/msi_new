<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository as Repository;
use App\Repositories\RoleRepository;
use App\Services\UsuariosServices as Service;

class UsuariosController extends Controller {

    use TraitController;

    protected $url = "usuarios";

    /**
     *
     * @var type 
     */
    protected $folderView = "usuarios.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "user";

    /**
     * 
     * @param Clientes $clientes
     */
    public function __construct(Repository $user, Service $service) {
        $this->repository = $user;
        $this->service = $service;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RoleRepository $roleRepository) {
        $roles = $roleRepository->all();
        return view($this->folderView . 'adicionar', compact('roles'));
    }

}
