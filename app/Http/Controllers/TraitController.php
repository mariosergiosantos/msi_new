<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Trait responsavel por implementar funções básicas dos controladores
 *
 * @author mario
 */
trait TraitController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $results = $this->repository->paginate(10);
        return view($this->folderView . 'index', compact('results', 'request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view($this->folderView . 'adicionar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $result = $this->service->create($request->all());
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.' . $this->entityLang));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function show($code) {
        $result = $this->service->show($code);
        //$this->authorize('show-product');
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return view($this->folderView . 'visualizar', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function edit($code) {
        $result = $this->service->show($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return view($this->folderView . 'editar', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code) {
        $result = $this->service->update($request->all(), $code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.update.' . $this->entityLang));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function destroy($code) {
        $result = $this->service->delete($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.destroy.' . $this->entityLang));
    }

    /**
     * 
     * @param Request $request
     */
    public function autoComplete(Request $request) {
        $term = $request->get('term');
        if (isset($term)) {
            $busca = strtolower($term);
            return $this->service->autoComplete($busca);
        }
    }

    /**
     * 
     * @param type $entidate
     * @param array $dados
     * @return type
     */
    public function storeAjax($entidate, array $dados) {
        $result = $entidate->create($dados);
        return $this->validadeRetornoAjax($result);
    }

    /**
     * 
     * @param type $entidade
     * @param type $code
     * @param array $dados
     * @return type
     */
    public function updateAjax($entidade, $code, array $dados) {
        $result = $entidade->update($dados, $code);
        return $this->validadeRetornoAjax($result);
    }

    /**
     * 
     * @param type $entidate
     * @param type $code
     * @return type
     */
    public function deleteAjax($entidate, $code) {
        $result = $entidate->delete($code);
        return $this->validadeRetornoAjax($result);
    }

    /**
     * 
     * @param type $result
     * @return type
     */
    private function validadeRetornoAjax($result) {
        if ($result['error']) {
            return response()->json([
                        'result' => false,
                        'message' => $result['messege']
                            ], 203);
        }
        return response()->json(['result' => true], 200);
    }

}
