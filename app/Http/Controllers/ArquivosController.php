<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DocumentoServices as Service;
use App\Repositories\DocumentosRepository as Repository;
use Illuminate\Filesystem\Filesystem;

class ArquivosController extends Controller {

    use TraitController;

    protected $url = "arquivos";

    /**
     *
     * @var type 
     */
    protected $folderView = "arquivos.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var \App\Services\DocumentoServices service 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $filesystem;

    /**
     *
     * @var type 
     */
    protected $entityLang = "file";

    /**
     *
     * @var type 
     */
    private $path;

    /**
     * @var Request $request
     */
    private $request;

    /**
     * 
     * @param Repository $repository
     * @param Service $service
     * @param Filesystem $filesystem
     * @param Request $request
     */
    public function __construct(Repository $repository, Service $service, Filesystem $filesystem, Request $request) {
        $this->repository = $repository;
        $this->service = $service;
        $this->filesystem = $filesystem;
        $this->path = public_path('assets/arquivos') . '/' . date('d-m-Y') . '/';
        $this->request = $request;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $results = $this->repository->paginate(10);
        
        /*if($request->has('q')){
            $results = $this->repository->findWhere([['documento', 'like', $request->get('q')]]);
        }*/
        
        return view($this->folderView . 'index', compact('results', 'request'));
    }
  
    /**
     * Store a newly created resource in storage.
     * @return Response
     */
    public function store() {
        $file = $this->request->file('userfile');
        $uploadArquivo = $this->uploadFile($file);

        if ($uploadArquivo['error']) {
            return redirect($this->url)->with("danger", $uploadArquivo['message']);
        }

        $anexo = $this->service->create($uploadArquivo['message']);
        if ($anexo['error']) {
            return redirect($this->url)->with("danger", $anexo['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.arquiver'));
    }

    /**
     * 
     * @param type $file
     * @return type
     */
    private function uploadFile($file) {
        if (!$this->validateExtension($file->getClientOriginalExtension())) {
            return [
                'error' => true,
                'message' => 'A extensão do arquivo não é permitida!'
            ];
        }

        $file->move($this->path, $file->getClientOriginalName());
        return [
            'error' => false,
            'message' => $this->formatDataArquiver($file)
        ];
    }

    /**
     * 
     * @param type $file
     */
    private function formatDataArquiver($file) {
        $nomeArquivo = $file->getClientOriginalName();
        $data = $this->request->all();
        $data['file'] = $nomeArquivo;
        $data['path'] = $this->path . $nomeArquivo;
        $data['url'] = url('assets/arquivos') . '/' . date('d-m-Y') . '/' . $nomeArquivo;
        $data['extensao'] = '.' . $file->getClientOriginalExtension();
        $data['tamanho'] = round((($file->getClientSize() / 1024) / 1000), 2);
        $data['documento'] = $this->request->get('nome');
        return $data;
    }

    /**
     * Verifica se a extensão do arquivo é valida
     * @param type $extension
     * @return boolean
     */
    private function validateExtension($extension) {
        $extensionValid = ['txt', 'pdf', 'gif', 'png', 'jpg', 'jpeg'];

        foreach ($extensionValid as $exensao) {
            if ($exensao == strtolower($extension)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code) {
        $result = $this->service->show($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $data = $request->all();
        $data['cadastro'] = $request->get('data');
        $this->service->update($data, $code);
        return redirect($this->url)->with("success", trans('messages.update.' . $this->entityLang));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function destroy($code) {
        $arquivo = $this->service->show($code);
        $result = $this->service->delete($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        if ($this->filesystem->exists($arquivo->path)) {
            $this->filesystem->delete($arquivo->path);
        }
        return redirect($this->url)->with("success", trans('messages.destroy.' . $this->entityLang));
    }

}
