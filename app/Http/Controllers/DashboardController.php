<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\Repositories\ProdutosRepository;
use App\Repositories\OsRepository;
use Illuminate\Database\DatabaseManager;
use App\Jobs\SendReminderEmail;

/**
 * Description of DashboardController
 *
 * @author mario
 */
class DashboardController extends Controller {
    
    private $produtos;
    
    private $ordens;
    
    public function __construct(ProdutosRepository $produtos, OsRepository $ordens) {
        $this->produtos = $produtos;
        $this->ordens = $ordens;
    }

    /**
     * 
     * @return type
     */
    public function index(DatabaseManager $db) {
        /*$produtos = $db->select($db->raw('SELECT * FROM produtos WHERE estoque <= estoque_minimo LIMIT 10'));
        $ordens = $this->ordens->findWhere(['status' => 'Aberto']);
        $estatisticas_financeiro = $db->select($db->raw("SELECT SUM(CASE WHEN recebido = 1 AND tipo = 'receita' THEN valor END) as total_receita, 
                       SUM(CASE WHEN recebido = 1 AND tipo = 'despesa' THEN valor END) as total_despesa,
                       SUM(CASE WHEN recebido = 0 AND tipo = 'receita' THEN valor END) as total_receita_pendente,
                       SUM(CASE WHEN recebido = 0 AND tipo = 'despesa' THEN valor END) as total_despesa_pendente FROM lancamentos"));
        
        //dd(collect([$estatisticas_financeiro, $produtos, $ordens]));
        //return view('mapos.painel', compact('produtos', 'ordens', 'estatisticas_financeiro'));
        //$this->dispatch(new SendReminderEmail("testando o envio do email atraves de queues"));
        return view('conecte.painel');
        */
        return view('angle.dashboard');
    }

}
