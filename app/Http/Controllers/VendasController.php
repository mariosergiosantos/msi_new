<?php

namespace App\Http\Controllers;

use App\Services\VendaServices as Service;
use App\Repositories\VendasRepository as Repository;
use App\Services\ItensDeVendaServices;
use App\Repositories\EmitenteRepository;
use Illuminate\Http\Request;
use App\Entities\FormaPagamentos;
use App\Entities\OpcoesParcelamento;

class VendasController extends Controller {

    use TraitController;

    protected $url = "vendas";

    /**
     *
     * @var type 
     */
    protected $folderView = "vendas.";

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $service;

    /**
     *
     * @var type 
     */
    protected $entityLang = "sale";

    /**
     *
     * @var type 
     */
    protected $produto;

    /**
     * 
     * @param Repository $repository
     * @param Service $service
     */
    public function __construct(Repository $repository, Service $service, ItensDeVendaServices $produtosServices) {
        $this->repository = $repository;
        $this->service = $service;
        $this->produto = $produtosServices;
    }

    /**
     * Display the specified resource.
     * @param EmitenteRepository $emitenteRepository
     * @param type $code
     * @return \Illuminate\Http\Response
     */
    public function show(EmitenteRepository $emitenteRepository, $code) {
        $result = $this->service->show($code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $emitente = $emitenteRepository->all();
        $produtos = $this->produto->show($result->idVendas);
        return view($this->folderView . 'visualizar', compact('result', 'produtos', 'emitente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function edit($code, FormaPagamentos $formaPagamentos) {
        $result = $this->service->show($code);
        if ($result->faturado == 1) {
            return redirect($this->url)->with('danger', 'Esta venda não pode ser editada, porque já foi finalizada!');
        }
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        $produtos = $this->produto->show($result->idVendas);
        $formaPagamentos = $formaPagamentos->all();
        return view($this->folderView . 'editar', compact('result', 'produtos', 'formaPagamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code) {
        $data = $request->all();
        $data['data_pagamento'] = dateCurrent();
        $result = $this->service->update($data, $code);
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return redirect($this->url. "/" . $result->idVendas)->with("success", trans('messages.update.' . $this->entityLang));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $result = $this->service->create($request->all());
        if ($result['error']) {
            return redirect($this->url)->with("danger", $result['messege']);
        }
        return redirect(url('vendas/' . $result->idVendas . '/edit'))->with("success", trans('messages.store.' . $this->entityLang));
    }
    
    public function getOrcamento(){
        return "Aqui vai ficar o orcamento de compras";
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function postProduto(Request $request) {
        return $this->storeAjax($this->produto, $request->all());
    }

    /**
     * 
     * @param type $idProduct
     * @return type
     */
    public function deleteProduto($idProduct) {
        return $this->deleteAjax($this->produto, $idProduct);
    }

    /**
     * Retorna uma lista de opções para parcelamento
     * @param OpcoesParcelamento $opcoesParcelamento
     * @param type $formaPagamento
     * @return type
     */
    public function opcoesParcelamento(OpcoesParcelamento $opcoesParcelamento, $formaPagamento) {
        $opcoes = $opcoesParcelamento->where('forma_pagamento_id', $formaPagamento)->get();
        return response()->json($opcoes, 200);
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function postFormaPagamento(Request $request) {
        return $this->updateAjax($this->repository, $request->get('idVendas'), $request->all());
    }

}
