<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ClientesRepository;
use App\Repositories\ProdutosRepository;
use App\Repositories\ServicosRepository;
use App\Repositories\OsRepository;
use App\Repositories\VendasRepository;
use App\Repositories\LancamentosRepository;

class RelatoriosController extends Controller {

    private $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * @param ClientesRepository $clientesRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClientes(ClientesRepository $clientesRepository) {
        if ($this->request->has('acao') && $this->request->get('acao') == 'imprimir') {
            $clientes = $clientesRepository->all();
            $pdf = \PDF::loadView('relatorios.imprimir.clientes', ['clientes' => $clientes]);
            return $pdf->stream();
        }
        return view('relatorios.clientes');
    }

    /**
     * @param ProdutosRepository $produtosRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProdutos(ProdutosRepository $produtosRepository) {
        if ($this->request->has('acao') && $this->request->get('acao') == 'imprimir') {
            $produtos = $produtosRepository->all();

            if ($this->request->has('precoInicial')) {
                $produtos = $produtos->filter(function ($item) {
                    return $item->precoVenda > $this->request->get('precoInicial');
                });
            }
            if ($this->request->has('precoFinal')) {
                $produtos = $produtos->filter(function ($item) {
                    return $item->precoVenda < $this->request->get('precoFinal');
                });
            }
            if ($this->request->has('estoqueInicial')) {
                $produtos = $produtos->filter(function ($item) {
                    return $item->estoque > $this->request->get('estoqueInicial');
                });
            }
            if ($this->request->has('estoqueFinal')) {
                $produtos = $produtos->filter(function ($item) {
                    return $item->estoque < $this->request->get('estoqueFinal');
                });
            }

            $pdf = \PDF::loadView('relatorios.imprimir.produtos', ['produtos' => $produtos]);
            return $pdf->stream();
        }
        return view('relatorios.produtos');
    }

    /**
     * @param ServicosRepository $servicosRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getServicos(ServicosRepository $servicosRepository) {
        $servicos = $servicosRepository->all();
        if ($this->request->has('precoInicial') && $this->request->has('precoFinal')) {
            //$servicos = $servicosRepository->findWhere(['preco' => $this->request->get('precoInicial')])->all();
            $servicos = $servicosRepository->findWhere([
                        ['preco', '>', $this->request->get('precoInicial')],
                        ['preco', '<', $this->request->get('precoFinal')]
                    ])->all();
        } else {
            
        }
        if ($this->request->get('acao') == 'imprimir') {
            $pdf = \PDF::loadView('relatorios.imprimir.servicos', array('servicos' => $servicos));
            return $pdf->stream();
        }
        return view('relatorios.servicos');
    }

    /**
     * @param OsRepository $osRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOs(OsRepository $osRepository) {
        if ($this->request->has('acao') && $this->request->get('acao') == 'imprimir') {
            $os = $osRepository->with(['cliente'])->all();
            $pdf = \PDF::loadView('relatorios.imprimir.os', ['os' => $os]);
            return $pdf->stream();
        }
        return view('relatorios.os');
    }

    /**
     * @param VendasRepository $vendasRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getVendas(VendasRepository $vendasRepository) {
        if ($this->request->has('acao') && $this->request->get('acao') == 'imprimir') {
            $vendas = $vendasRepository->with(['cliente', 'usuario'])->all();
            $pdf = \PDF::loadView('relatorios.imprimir.vendas', ['vendas' => $vendas]);
            return $pdf->stream();
        }
        return view('relatorios.vendas');
    }

    /**
     * @param LancamentosRepository $lancamentosRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFinanceiro(LancamentosRepository $lancamentosRepository) {
        if ($this->request->has('acao') && $this->request->get('acao') == 'imprimir') {
            $lancamentos = $lancamentosRepository->all();

            if ($this->request->has('tipo')) {
                $lancamentos = $lancamentos->where('tipo', $this->request->get('tipo'));
            }

            $pdf = \PDF::loadView('relatorios.imprimir.financeiro', ['lancamentos' => $lancamentos]);
            return $pdf->stream();
        }
        return view('relatorios.financeiro');
    }

}
