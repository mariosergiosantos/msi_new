<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "configuration" => [
        "success" => "Configurações atualizadas com sucesso!",
        "error" => "Erro oa atualizar as configurações!"
    ],
    "nofound" => [
        "product" => "Produto não encontrado!",
        "provider" => "Fornecedor não encontrado!",
        "client" => "Cliente não encontrado!",
        "user" => "Usuário não encontrado!",
        "service" => "Serviço não encontrado!",
        "orderService" => "Ordem de serviço não encontrado!",
        "sale" => "Venda não encontrada!",
        "file" => "Arquivo não encontrado!",
        "releases" => "Lançamento não encontrado!",
        "issuer" => "Entidade não encontrada!",
    ],
    "store" => [
        "product" => "Produto cadastrado!",
        "provider" => "Fornecedor cadastrado!",
        "client" => "Cliente cadastrado!",
        "user" => "Usuário cadastrado!",
        "service" => "Serviço cadastrado!",
        "orderService" => "Ordem de serviço cadastrado!",
        "sale" => "Venda cadastrada!",
        "file" => "Arquivo cadastrado!",
        "releases" => "Lançamento cadastrado!",
        "issuer" => "Entidade Cadastrada!",
    ],
    "update" => [
        "product" => "Produto atualizado!",
        "provider" => "Fornecedor atualizado!",
        "client" => "Cliente atualizado!",
        "user" => "Usuário atualizado!",
        "service" => "Serviço atualizado!",
        "orderService" => "Ordem de serviço atualizado!",
        "sale" => "Vendas atualizado!",
        "file" => "Arquivo atualizado!",
        "releases" => "Lançamento atualizado!",
        "issuer" => "Entidade atualizada!",
    ],
    "destroy" => [
        "product" => "Produto excluído!",
        "provider" => "Fornecedor excluído!",
        "client" => "Cliente excluído!",
        "user" => "Usuário excluído!",
        "service" => "Serviço excluído!",
        "orderService" => "Ordem de serviço excluído!",
        "sale" => "Vendas excluído!",
        "file" => "Arquivo excluído!",
        "releases" => "Lançamento excluído!",
        "issuer" => "Entidade Excluida!",
    ],
    "page" => [
        "product" => "Produto",
        "provider" => "Fornecedor",
        "client" => "Cliente",
        "user" => "Usuário",
        "service" => "Serviço",
        "dashboard" => "Painel",
        "configuration" => "Configuração",
        "orderService" => "Ordem de serviço",
        "sale" => "Vendas",
        "file" => "Arquivo",
        "releases" => "Lançamentos Financeiros",
        "reports" => "Relatórios",
        "issuer" => "Entidade",
    ],
    
    "notExclud" => [
        "product" => "Este produto não pode ser excluido por ter relação com alguma ordem de seviço e/ou venda",
        "provider" => "Este fornecedor não pode ser excluido por com algum produto",
        "client" => "Cliente",
        "user" => "Usuário",
        "orderService" => "Ordem de serviço",
    ]
];
