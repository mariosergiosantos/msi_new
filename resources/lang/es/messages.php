<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "configuration" => [
        "success" => "Ha actualizado correctamente la configuración!",
        "error" => "Error al actualizar la configuración!"
    ],
    "nofound" => [
        "product" => "Producto no encontrado!",
        "provider" => "Proveedor no encontrado!",
        "client" => "El cliente no encontrado!",
        "user" => "Usuario no encontrado!",
        "service" => "servicio no encontrado!",
        "orderService" => "Orden de servicio no encontrado!",
        "sale" => "Venta no encontrado!",
        "file" => "Expediente no encontrado!",
        "releases" => "Lanzamientos no encontrado!",
        "issuer" => "entidad no encontrado!",
    ],
    "store" => [
        "product" => "Producto registrado!",
        "provider" => "Proveedor de registro!",
        "client" => "Cliente registrado!",
        "user" => "Usuario no encontrado!",
        "service" => "servicio no encontrado!",
        "orderService" => "Orden de servicio no encontrado!",
        "sale" => "Venta no encontrado!",
        "file" => "Expediente no encontrado!",
        "releases" => "Lanzamientos no encontrado!",
        "issuer" => "Entidad no encontrado!",
    ],
    "update" => [
        "product" => "Actualizado producto!",
        "provider" => "Proveedor actualizado!",
        "client" => "Client actualiza!",
        "user" => "Usuario actualizado!",
        "service" => "servicio actualizado!",
        "orderService" => "Orden de servicio actualizado!",
        "sale" => "Venta actualizado",
        "file" => "expediente actualizado",
        "releases" => "Lanzamientos actualizado",
        "issuer" => "Entidad actualizado!",
    ],
    "destroy" => [
        "product" => "Producto eliminados!",
        "provider" => "Proveedor eliminados!",
        "client" => "Cliente borrado!",
        "user" => "Usuario eliminados!",
        "service" => "servicio eliminados!",
        "orderService" => "Orden de servicio eliminados!",
        "sale" => "Venta eliminados!",
        "file" => "Expediente eliminados!",
        "releases" => "Lanzamientos eliminados!",
        "issuer" => "Entidad eliminados!",
    ],
    "page" => [
        "product" => "Producto",
        "provider" => "Proveedor",
        "client" => "Cliente",
        "user" => "Usuario",
        "dashboard" => "Panel de control",
        "configuration" => "Configuración",
        "orderService" => "Orden de servicio",
        "sale" => "Venta",
        "file" => "Files",
        "releases" => "Expediente",
        "reports" => "Lanzamientos",
        "issuer" => "Entidad",
    ],
];
