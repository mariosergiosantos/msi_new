<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "configuration" => [
        "success" => "Successfully updated settings!",
        "error" => "Error updating settings!"
    ],
    "nofound" => [
        "product" => "Product not found!",
        "provider" => "Provider not found!",
        "client" => "Client not found!",
        "user" => "User not found!",
        "service" => "Service not found!",
        "orderService" => "Order Service not found!",
        "sale" => "Sale not found!",
        "file" => "File not found!",
        "releases" => "Release not found!",
        "issuer" => "Issuer not found!",
    ],
    "store" => [
        "product" => "Product registered!",
        "provider" => "Provider registered!",
        "client" => "Client registered!",
        "user" => "User registered!",
        "service" => "Service registered!",
        "orderService" => "Order Service registered!",
        "sale" => "Sale registered!",
        "file" => "File registered!",
        "releases" => "Releases registered!",
        "issuer" => "Issuer registered!",
    ],
    "update" => [
        "product" => "Product updated!",
        "provider" => "Provider updated!",
        "client" => "Client updated!",
        "user" => "User updated!",
        "service" => "Service updated!",
        "orderService" => "Order Service updated!",
        "sale" => "Sale updated!",
        "file" => "File updated!",
        "releases" => "Releases updated!",
        "issuer" => "Issuer updated!",
    ],
    "destroy" => [
        "product" => "Product excluded!",
        "provider" => "Provider excluded!",
        "client" => "Client excluded!",
        "user" => "User excluded!",
        "service" => "Service excluded!",
        "orderService" => "Order Service excluded!",
        "sale" => "Sale excluded!",
        "file" => "File excluded!",
        "releases" => "Releases excluded!",
        "issuer" => "Issuer excluded!",
    ],
    "page" => [
        "product" => "Product",
        "provider" => "Provider",
        "client" => "Client",
        "user" => "User",
        "service" => "Service",
        "dashboard" => "Dashboard",
        "configuration" => "configuration",
        "orderService" => "Order of Service",
        "sale" => "Sales",
        "file" => "Files",
        "releases" => "Releases",
        "reports" => "Reports",
        "issuer" => "Issuer",
    ],
];
