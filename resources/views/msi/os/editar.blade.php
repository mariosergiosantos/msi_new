@extends('tema.topo')
@section('conteudo')
<link rel="stylesheet" href="{{url('js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css')}}"/>
<script type="text/javascript" src="{{url('js/jquery-ui/js/jquery-ui-1.9.2.custom.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.validate.js')}}"></script>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Editar OS</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="col-md-12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a>
                        </li>
                        <li id="tabProdutos"><a href="#tab2" data-toggle="tab">Produtos</a></li>
                        <li id="tabServicos"><a href="#tab3" data-toggle="tab">Serviços</a></li>
                        <li id="tabAnexos"><a href="#tab4" data-toggle="tab">Anexos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="col-md-12" id="divCadastrarOs">
                                <form action="{{url('')}}" method="post" id="formOs">
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <h3>#Protocolo: <?php echo $result->idOs ?></h3>
                                        <div class="col-md-6" style="margin-left: 0">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="col-md-12 form-control" disabled type="text"
                                                   name="cliente"
                                                   value="<?php echo $result->cliente['nome'] ?>"/>
                                            <input id="clientes_id" class="col-md-12" type="hidden"
                                                   name="clientes_id"
                                                   value="<?php echo $result->cliente['idClientes'] ?>"/>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tecnico">Técnico / Responsável<span
                                                    class="required">*</span></label>
                                            <input id="tecnico" class="col-md-12 form-control" disabled type="text"
                                                   name="tecnico" value="<?php echo $result->usuario['nome'] ?>"/>
                                            <input id="usuarios_id" class="col-md-12" type="hidden"
                                                   name="usuarios_id"
                                                   value="<?php echo $result->usuario['idUsuarios'] ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-3">
                                            <label for="status">Status<span class="required">*</span></label>
                                            <select class="col-md-12 form-control" name="status" id="status"
                                                    value="">
                                                <option <?php
                                                if ($result->status == 'Orçamento') {
                                                    echo 'selected';
                                                }
                                                ?> value="Orçamento">Orçamento
                                                </option>
                                                <option <?php
                                                if ($result->status == 'Aberto') {
                                                    echo 'selected';
                                                }
                                                ?> value="Aberto">Aberto
                                                </option>
                                                <option <?php
                                                if ($result->status == 'Faturado') {
                                                    echo 'selected';
                                                }
                                                ?> value="Faturado">Faturado
                                                </option>
                                                <option <?php
                                                if ($result->status == 'Finalizado') {
                                                    echo 'selected';
                                                }
                                                ?> value="Finalizado">Finalizado
                                                </option>
                                                <option <?php
                                                if ($result->status == 'Cancelado') {
                                                    echo 'selected';
                                                }
                                                ?> value="Cancelado">Cancelado
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dataInicial">Data Inicial<span
                                                    class="required">*</span></label>
                                            <input id="dataInicial" class="col-md-12 datepicker form-control"
                                                   type="text" name="dataInicial"
                                                   value="<?php echo date('d/m/Y', strtotime($result->dataInicial)); ?>"/>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dataFinal">Data Final</label>
                                            <input id="dataFinal" class="col-md-12 datepicker form-control"
                                                   type="text" name="dataFinal"
                                                   value="<?php echo date('d/m/Y', strtotime($result->dataFinal)); ?>"/>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="garantia">Garantia</label>
                                            <input id="garantia" type="text" class="col-md-12 form-control"
                                                   name="garantia" value="<?php echo $result->garantia ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">

                                        <div class="col-md-6">
                                            <label for="descricaoProduto">Descrição Produto/Serviço</label>
                                            <textarea class="col-md-12 form-control" name="descricaoProduto"
                                                      id="descricaoProduto" cols="30"
                                                      rows="5"><?php echo $result->descricaoProduto ?></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="defeito">Defeito</label>
                                            <textarea class="col-md-12 form-control" name="defeito" id="defeito"
                                                      cols="30" rows="5"><?php echo $result->defeito ?></textarea>
                                        </div>

                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-6">
                                            <label for="observacoes">Observações</label>
                                            <textarea class="col-md-12 form-control" name="observacoes"
                                                      id="observacoes" cols="30"
                                                      rows="5"><?php echo $result->observacoes ?></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="laudoTecnico">Laudo Técnico</label>
                                            <textarea class="col-md-12 form-control" name="laudoTecnico"
                                                      id="laudoTecnico" cols="30"
                                                      rows="5"><?php echo $result->laudoTecnico ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-6 offset3" style="text-align: center">
                                            <?php if ($result->faturado == 0) { ?>
                                                <a href="#modal-faturar" id="btn-faturar" role="button"
                                                   data-toggle="modal" class="btn btn-success"><i class="icon-file"></i>
                                                    Faturar</a>
                                            <?php } ?>
                                            <button class="btn btn-primary" id="btnContinuar"><i
                                                    class="icon-white icon-ok"></i> Alterar
                                            </button>
                                            <a href="{{url('os')}}/<?php echo $result->idOs; ?>"
                                               class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar
                                                OS</a>
                                            <a href="{{url('os')}}" class="btn"><i class="icon-arrow-left"></i>
                                                Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--Produtos-->
                        <div class="tab-pane" id="tab2">
                            <div class="col-md-12 well" style="padding: 1%; margin-left: 0">
                                <form id="formProdutos" action="{{url('os/editarProduto')}}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="col-md-8">
                                        <input type="hidden" name="produtos_id" id="produtos_id"/>
                                        <input type="hidden" name="os_id" id="os_id"
                                               value="<?php echo $result->idOs ?>"/>
                                        <input type="hidden" name="estoque" id="estoque" value=""/>
                                        <input type="hidden" name="preco" id="preco" value=""/>
                                        <label for="">Produto</label>
                                        <input type="text" class="col-md-12 form-control" name="produto"
                                               id="produto" placeholder="Digite o nome do produto"/>
                                    </div>
                                    <input id="subTotal" type="hidden" name="subTotal" value=""/>
                                    <div class="col-md-2">
                                        <label for="">Quantidade</label>
                                        <input type="text" placeholder="Quantidade" id="quantidade"
                                               name="quantidade" class="col-md-12 form-control"/>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">.</label>
                                        <button type="submit" class="btn btn-success col-md-12" id="btnAdicionarProduto"><i
                                                class="icon-white icon-plus"></i> Adicionar
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12" id="divProdutos" style="margin-left: 0">
                                <table class="table table-bordered" id="tblProdutos">
                                    <thead>
                                        <tr>
                                            <th>Produto</th>
                                            <th>Quantidade</th>
                                            <th>Ações</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $total = 0;
                                        foreach ($produtosOs as $p) {
                                            $total = $total + $p->subTotal;
                                            echo '<tr>';
                                            echo '<td>' . $p->produto['nome'] . '</td>';
                                            echo '<td>' . $p->quantidade . '</td>';
                                            echo '<td><a href="" idAcao="' . $p->idProdutos_os . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '" title="Excluir Produto" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                            echo '<td>R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                            echo '</tr>';
                                        }
                                        ?>

                                        <tr>
                                            <td colspan="3" style="text-align: right"><strong>Sub-Total:</strong></td>
                                            <td><strong>R$ {{ number_format($total, 2, ',', '.') }}
                                                    <input type="hidden" id="total-venda" value="{{ number_format($total, 2) }}"></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!--Serviços-->
                        <div class="tab-pane" id="tab3">
                            <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                <div class="col-md-12 well" style="padding: 1%; margin-left: 0">
                                    <form id="formServicos" action="{{url('os/adicionarServico')}}" method="post">
                                        {!! csrf_field() !!}
                                        <div class="col-md-10">
                                            <input type="hidden" name="servicos_id" id="servicos_id"/>
                                            <input type="hidden" name="os_id" id="os_id"
                                                   value="<?php echo $result->idOs ?>"/>
                                            <input type="hidden" name="subTotal" id="subTotalServico" value=""/>
                                            <label for="">Serviço</label>
                                            <input type="text" class="col-md-12 form-control" name="servico"
                                                   id="servico" placeholder="Digite o nome do serviço"/>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="">.</label>
                                            <button type="submit" class="btn btn-success col-md-12"><i
                                                    class="icon-white icon-plus"></i> Adicionar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12" id="divServicos" style="margin-left: 0">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Serviço</th>
                                                <th>Ações</th>
                                                <th>Sub-total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total = 0;
                                            foreach ($servicosOs as $s) {
                                                $preco = $s->subTotal;
                                                $total = $total + $preco;
                                                echo '<tr>';
                                                echo '<td>' . $s->servico['nome'] . '</td>';
                                                echo '<td><span idAcao="' . $s->idServicos_os . '" title="Excluir Serviço" class="btn btn-danger"><i class="icon-remove icon-white"></i></span></td>';
                                                echo '<td>R$ ' . number_format($s->subTotal, 2, ',', '.') . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>

                                            <tr>
                                                <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                                <td><strong>R$ <?php echo number_format($total, 2, ',', '.'); ?><input
                                                            type="hidden" id="total-servico"
                                                            value="<?php echo number_format($total, 2); ?>"></strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--Anexos-->
                        <div class="tab-pane" id="tab4">
                            <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                <div class="col-md-12 well" style="padding: 1%; margin-left: 0" id="form-anexos">
                                    <form id="formAnexos" enctype="multipart/form-data" action="javascript:;"
                                          accept-charset="utf-8" s method="post">
                                        {!! csrf_field() !!}
                                        <div class="col-md-10">
                                            <input type="hidden" name="idOsServico" id="idOsServico"
                                                   value="<?php echo $result->idOs ?>"/>
                                            <label for="">Anexo</label>
                                            <input type="file" class="col-md-12" name="userfile[]"
                                                   multiple="multiple" size="20"/>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="">.</label>
                                            <button type="submit" class="btn btn-success col-md-12"><i
                                                    class="icon-white icon-plus"></i> Anexar
                                            </button>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-md-12" id="divAnexos" style="margin-left: 0">
                                    <?php
                                    $cont = 1;
                                    $flag = 5;
                                    foreach ($result->anexos as $a) {

                                        if ($a->thumb == null) {
                                            $thumb = 'assets/img/icon-file.png';
                                            $link = 'assets/img/icon-file.png';
                                        } else {
                                            $thumb = 'assets/anexos/thumbs/' . $a->thumb;
                                            $link = $a->url . $a->anexo;
                                        }

                                        if ($cont == $flag) {
                                            echo '<div style="margin-left: 0" class="col-md-3"><a href="#modal-anexo" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                            $flag += 4;
                                        } else {
                                            echo '<div class="col-md-3"><a href="#modal-anexo" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                        }
                                        $cont++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                .
            </div>
        </div>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div class="modal fade" id="modal-anexo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Visualizar Anexo</h3>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="div-visualizar-anexo" style="text-align: center">
                    <div class='progress progress-info progress-striped active'>
                        <div class='bar' style='width: 100%'></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
                <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal Faturar-->
<!--<div class="modal fade" id="modal-faturar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="formFaturar" action="{{url('')}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Faturar Venda</h3>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos
                        com asterisco.
                    </div>
                    <div class="form-group" style="margin-left: 0">
                        <label for="descricao">Descrição</label>
                        <input class="form-control"
                               id="descricao"
                               type="text"
                               name="descricao"
                               value="Fatura de Venda - #<?php echo $result->idOs; ?> "/>

                    </div>
                    <div class="form-group" style="margin-left: 0">
                        <label for="cliente">Cliente*</label>
                        <input class="form-control"
                               id="cliente"
                               type="text"
                               name="cliente"
                               value="<?php echo $result->nomeCliente ?>"/>
                        <input type="hidden" 
                               name="clientes_id"
                               id="clientes_id"
                               value="<?php echo $result->clientes_id ?>">
                        <input type="hidden" name="os_id" id="os_id"
                               value="<?php echo $result->idOs; ?>">
                    </div>
                    <div class="form-group" >
                        <div class="col-md-4" style="margin-left: 0">
                            <label for="valor">Valor*</label>
                            <input type="hidden" id="tipo" name="tipo" value="receita"/>
                            <input class="form-control money" id="valor" type="text" name="valor"/>
                        </div>
                        <div class="col-md-4">
                            <label for="vencimento">Data Vencimento*</label>
                            <input class="form-control datepicker" id="vencimento" type="text"
                                   name="data_vencimento"/>
                        </div>
                    </div>

                    <div class="form-group" style="margin-left: 0">
                        <div class="col-md-4" style="margin-left: 0">
                            <label for="recebido">Recebido?</label>
                            &nbsp &nbsp &nbsp &nbsp<input id="recebido" class="" type="checkbox" name="recebido"
                                                          value="1"/>
                        </div>
                        <div id="divRecebimento" class="col-md-8" style=" display: none">
                            <div class="col-md-6">
                                <label for="recebimento">Data Recebimento</label>
                                <input class="form-control datepicker" id="recebimento" type="text"
                                       name="data_pagamento"/>
                            </div>
                            <div class="col-md-6">
                                <label for="formaPgto">Forma de Pagamento</label>
                                <select name="forma_pgto" id="formaPgto" class="form-control">
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="Cartão de Crédito">Cartão de Crédito</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Boleto">Boleto</option>
                                    <option value="Depósito">Depósito</option>
                                    <option value="Débito">Débito</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
                    <button class="btn btn-primary">Faturar</button>
                </div>
            </form>
        </div>
    </div>
</div>-->

<div class="modal fade" id="modal-faturar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="formFaturar" action="{{url('lancamentos')}}" method="post" class="form-horizontal">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Adicionar Receita</h3>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos
                        campos com asterisco.
                    </div><hr>

                    <div class="form-group span12" style="margin-left: 0">
                        <label for="descricao">Descrição</label>
                        <input class="form-control" id="descricao" type="text" name="descricao"/>
                    </div>
                    <div class="form-group span12" style="margin-left: 0">
                        <label for="cliente">Cliente*</label>
                        <input class="form-control" id="cliente" type="text" name="cliente_fornecedor"/>
                    </div>
                    <div class="form-group" >
                        <div class="col-md-4" style="margin-left: 0">
                            <label for="valor">Valor*</label>
                            <input type="hidden" id="tipo" name="tipo" value="receita"/>
                            <input class="form-control money" id="valor" type="text" name="valor"/>
                        </div>
                        <div class="col-md-4">
                            <label for="vencimento">Data Vencimento*</label>
                            <input class="form-control datepicker" id="vencimento" type="text"
                                   name="data_vencimento"/>
                        </div>
                    </div>
                    <div class="form-group" style="margin-left: 0">
                        <div class="col-md-4" style="margin-left: 0">
                            <label for="recebido">Recebido?</label>
                            &nbsp &nbsp &nbsp &nbsp<input id="recebido" class="" type="checkbox" name="recebido"
                                                          value="1"/>
                        </div>
                        <div id="divRecebimento" class="col-md-8" style=" display: none">
                            <div class="col-md-6">
                                <label for="recebimento">Data Recebimento</label>
                                <input class="form-control datepicker" id="recebimento" type="text"
                                       name="data_pagamento"/>
                            </div>
                            <div class="col-md-6">
                                <label for="formaPgto">Forma de Pagamento</label>
                                <select name="forma_pgto" id="formaPgto" class="form-control">
                                    <option value="Dinheiro">Dinheiro</option>
                                    <option value="Cartão de Crédito">Cartão de Crédito</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Boleto">Boleto</option>
                                    <option value="Depósito">Depósito</option>
                                    <option value="Débito">Débito</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-success">Faturar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{url('js/jquery.validate.js')}}"></script>
<script src="{{url('js/maskmoney.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(".money").maskMoney();
    $('#recebido').click(function (event) {
        var flag = $(this).is(':checked');
        if (flag == true) {
            $('#divRecebimento').show();
        }
        else {
            $('#divRecebimento').hide();
        }
    });

    $(document).on('click', '#btn-faturar', function (event) {
        event.preventDefault();
        valor = $('#total-venda').val();
        total_servico = $('#total-servico').val();
        valor = valor.replace(',', '');
        total_servico = total_servico.replace(',', '');
        total_servico = parseFloat(total_servico);
        valor = parseFloat(valor);
        $('#valor').val(valor + total_servico);
    });
    $("#formOs").validate({
        rules: {
            cliente: {required: true},
            tecnico: {required: true},
            dataInicial: {required: true}
        },
        messages: {
            cliente: {required: 'Campo Requerido.'},
            tecnico: {required: 'Campo Requerido.'},
            dataInicial: {required: 'Campo Requerido.'}
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    $("#formProdutos").validate({
        rules: {
            produto: {required: true},
            quantidade: {required: true}
        },
        messages: {
            produto: {required: "Selecione um produto"},
            quantidade: {required: 'Insira a quantidade'}},
        submitHandler: function (form) {
            var quantidade = parseInt($("#quantidade").val());
            $("#subTotal").val(parseFloat($("#preco").val()) * quantidade);
            var dados = $(form).serialize();
            $("#divProdutos").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>");
            $.ajax({
                type: "POST",
                url: "{{url('os/produto')}}",
                data: dados,
                dataType: 'json',
                success: function (data) {
                    if (!data.result) {
                        alert(data.message);
                        return false;
                    }
                    $("#quantidade").val('');
                    $("#produto").val('').focus();
                    $("#divProdutos").load("{{request()->getUri()}} #divProdutos");
                }
            });
            return false;
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    $("#formServicos").validate({
        rules: {
            servico: {required: true}
        },
        messages: {
            servico: {required: "Selecione um servico"}},
        submitHandler: function (form) {
            var dados = $(form).serialize();
            $("#divServicos").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>");
            $.ajax({
                type: "POST",
                url: "{{url('os/servico')}}",
                data: dados,
                dataType: 'json',
                success: function (data) {
                    if (!data.result) {
                        alert(data.message);
                        return false;
                    }
                    $("#servico").val('').focus();
                    $("#divServicos").load("{{request()->getUri()}} #divServicos");
                }
            });
            return false;
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
    $(document).on('click', 'a', function (event) {
        var idProduto = $(this).attr('idAcao');
        if ((idProduto % 1) == 0) {
            $("#divProdutos").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>");
            $.ajax({
                type: "DELETE",
                url: "{{url('os/produto')}}" + '/' + idProduto,
                dataType: 'json',
                success: function (data) {
                    if (!data.result) {
                        alert(data.message);
                    }
                    $("#divProdutos").load("{{request()->getUri()}} #divProdutos");
                }
            });
            return false;
        }
    });

    $(document).on('click', 'span', function (event) {
        var idServico = $(this).attr('idAcao');
        if ((idServico % 1) == 0) {
            $("#divServicos").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>");
            $.ajax({
                type: "DELETE",
                url: "{{url('os/servico')}}" + '/' + idServico,
                dataType: 'json',
                success: function (data) {
                    if (!data.result) {
                        alert(data.message);
                    }
                    $("#divServicos").load("{{request()->getUri()}} #divServicos");
                }
            });
            return false;
        }
    });

    $("#formAnexos").validate({
        submitHandler: function (form) {
            //var dados = $( form ).serialize();
            var dados = new FormData(form);
            $("#form-anexos").hide('1000');
            /*$("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
             $.ajax({
             type: "POST",
             url: "{{url('os/anexo')}}",
             data: dados,
             mimeType: "multipart/form-data",
             contentType: false,
             cache: false,
             processData: false,
             dataType: 'json',
             success: function (data) {
             if (data.result == true) {
             $("#divAnexos").load("#divAnexos");
             $("#userfile").val('');
             }
             else {
             $("#divAnexos").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> ' + data.mensagem + '</div>');
             }
             },
             error: function () {
             $("#divAnexos").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você anexou o(s) arquivo(s).</div>');
             }
             });
             $("#form-anexos").show('1000');
             return false;*/
        }
    });

    $(document).on('click', '.anexo', function (event) {
        event.preventDefault();
        var link = $(this).attr('link');
        var id = $(this).attr('imagem');
        var url = 'os/excluirAnexo/';
        $("#div-visualizar-anexo").html('<img src="' + link + '" alt="">');
        $("#excluir-anexo").attr('link', url + id);
        $("#download").attr('href', "{{url('os/downloadanexo')}}/" + id);
    });

    $(document).on('click', '#excluir-anexo', function (event) {
        event.preventDefault();

        var link = $(this).attr('link');
        $('#modal-anexo').modal('hide');
        $("#divAnexos").html("<div class='progress progress-info progress-striped active active'><div class='bar' style='width: 100%'></div></div>");

        $.ajax({type: "POST",
            url: link,
            dataType: 'json',
            success: function (data) {
                if (data.result == true) {
                    $("#divAnexos").load("#divAnexos");
                }
                else {
                    alert(data.mensagem);
                }
            }
        });
    });

    $("#formFaturar").validate({
        rules: {
            descricao: {required: true},
            cliente: {required: true},
            valor: {required: true},
            vencimento: {required: true}
        }, messages: {
            descricao: {required: 'Campo Requerido.'},
            cliente: {required: 'Campo Requerido.'},
            valor: {required: 'Campo Requerido.'},
            vencimento: {required: 'Campo Requerido.'}},
        submitHandler: function (form) {
            var dados = $(form).serialize();
            $('#btn-cancelar-faturar').trigger('click');
            /*$.ajax({
             type: "POST",
             url: "index.php/os/faturar",
             data: dados,
             dataType: 'json',
             success: function (data) {
             if (data.result == true) {
             
             window.location.reload(true);
             }
             else {
             alert('Ocorreu um erro ao tentar faturar OS.');
             $('#progress-fatura').hide();
             }
             }
             });
             return false;*/
        }
    });


//AutoComplete

    $("#produto").autocomplete({
        source: "{{url('autocomplete/produto')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#produtos_id").val(ui.item.id);
            $("#estoque").val(ui.item.estoque);
            $("#preco").val(ui.item.preco);
            $("#quantidade").focus();
        }
    });

    $("#servico").autocomplete({
        source: "{{url('autocomplete/servico')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#servicos_id").val(ui.item.id);
            $("#subTotalServico").val(ui.item.preco);
        }
    });

    $("#cliente").autocomplete({
        source: "{{url('autocomplete/cliente')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#clientes_id").val(ui.item.id);
        }
    });

    $("#tecnico").autocomplete({
        source: "{{url('autocomplete/usuario')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#usuarios_id").val(ui.item.id);
        }
    });

    $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
});
</script>
@endsection
