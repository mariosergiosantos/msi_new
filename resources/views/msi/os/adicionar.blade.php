@extends('tema.topo')
@section('conteudo')
<link rel="stylesheet" href="{{url('js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css')}}" />
<script type="text/javascript" src="{{url('js/jquery-ui/js/jquery-ui-1.9.2.custom.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.validate.js')}}"></script>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Cadastro de OS</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="col-md-12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="col-md-12" id="divCadastrarOs">
                                @include('errors.mensagem')
                                <form action="{{url('os')}}" method="post" id="formOs">
                                    {!! csrf_field() !!}
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-6">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="col-md-12 form-control" type="text" name="cliente" value=""  />
                                            <input id="clientes_id" class="col-md-12" type="hidden" name="clientes_id" value="1"  />
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tecnico">Técnico / Responsável<span class="required">*</span></label>
                                            <input id="tecnico" class="col-md-12 form-control" type="text" name="tecnico" value=""  />
                                            <input id="usuarios_id" class="col-md-12" type="hidden" name="usuarios_id" value="1"  />
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-3">
                                            <label for="status">Status<span class="required">*</span></label>
                                            <select class="col-md-12 form-control" name="status" id="status" value="">
                                                <option value="Orçamento">Orçamento</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dataInicial">Data Inicial<span class="required">*</span></label>
                                            <input id="dataInicial" class="col-md-12 datepicker form-control" type="text" name="dataInicial" value=""  />
                                        </div>
                                        <div class="col-md-3">
                                            <label for="dataFinal">Data Final</label>
                                            <input id="dataFinal" class="col-md-12 datepicker form-control" type="text" name="dataFinal" value=""  />
                                        </div>
                                        <div class="col-md-3">
                                            <label for="garantia">Garantia</label>
                                            <input id="garantia" type="text" class="col-md-12 form-control" name="garantia" value=""  />
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-6">
                                            <label for="descricaoProduto">Descrição Produto/Serviço</label>
                                            <textarea class="col-md-12 form-control" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="defeito">Defeito</label>
                                            <textarea class="col-md-12 form-control" name="defeito" id="defeito" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-6">
                                            <label for="observacoes">Observações</label>
                                            <textarea class="col-md-12 form-control" name="observacoes" id="observacoes" cols="30" rows="5"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="laudoTecnico">Laudo Técnico</label>
                                            <textarea class="col-md-12 form-control" name="laudoTecnico" id="laudoTecnico" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-6 offset3" style="text-align: center">
                                            <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button>
                                            <a href="{{url('os')}}" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                .
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    $("#cliente").autocomplete({
        source: "{{url('autocomplete/cliente')}}",
        minLength: 1,
        select: function (event, ui) {
            $("#clientes_id").val(ui.item.id);
        }
    });

    $("#tecnico").autocomplete({
        source: "{{url('autocomplete/usuario')}}",
        minLength: 1,
        select: function (event, ui) {
            $("#usuarios_id").val(ui.item.id);
        }
    });
    $("#formOs").validate({
        rules: {
            cliente: {required: true},
            tecnico: {required: true},
            dataInicial: {required: true}
        },
        messages: {
            cliente: {required: 'Campo Requerido.'},
            tecnico: {required: 'Campo Requerido.'},
            dataInicial: {required: 'Campo Requerido.'}
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
});
</script>
@endsection
