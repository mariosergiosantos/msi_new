@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            @include('errors.mensagem')
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Ordem de Serviço</h5>
                <div class="buttons">
                    <a title="Icon Title" class="btn btn-mini btn-info" href="{{$result->idOs}}/edit"><i class="icon-pencil icon-white"></i> Editar</a>
                    <a id="imprimir" title="Imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i> Imprimir</a>
                </div>
            </div>
            <div class="widget-content" id="printOs">
                <div class="invoice-content">
                    <div class="invoice-head" style="margin-bottom: 0">
                        <table class="table">
                            <tbody>
                                <?php
                                if ($emitente == null) {
                                    ?>
                                    <tr>
                                        <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="{{url('emitente')}}">Configurar</a><<<</td>
                                    </tr>
                                <?php } else { ?>

                                    <tr>
                                        <td style="width: 25%"><img src=" <?php echo $emitente[0]->url_logo; ?> "></td>
                                        <td> <span style="font-size: 20px; "> <?php echo $emitente[0]->nome; ?></span> </br><span><?php echo $emitente[0]->cnpj; ?> </br> <?php echo $emitente[0]->rua . ', nº:' . $emitente[0]->numero . ', ' . $emitente[0]->bairro . ' - ' . $emitente[0]->cidade . ' - ' . $emitente[0]->uf; ?> </span> </br> <span> E-mail: <?php echo $emitente[0]->email . ' - Fone: ' . $emitente[0]->telefone; ?></span></td>
                                        <td style="width: 18%; text-align: center">#Venda: <span ><?php echo $result->idVendas ?></span></br> </br> <span>Emissão: <?php echo date('d/m/Y'); ?></span></td>
                                    </tr>

                                <?php } ?>
                            </tbody>
                        </table>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 50%; padding-left: 0">
                                        <ul>
                                            <li>
                                                <span><h5>Cliente</h5>
                                                    <span>{{$result->cliente['nome']}}</span><br/>
                                                    <span>{{$result->cliente['rua']}}, {{$result->cliente['numero']}}, {{$result->cliente['bairro']}}</span><br/>
                                                    <span>{{$result->cliente['cidade']}} - {{$result->cliente['estado']}}</span>
                                            </li>
                                        </ul>
                                    </td>
                                    <td style="width: 50%; padding-left: 0">
                                        <ul>
                                            <li>
                                                <span><h5>Responsável</h5></span>
                                                <span>{{$result->usuario['nome']}}</span> <br/>
                                                <span>Telefone: {{$result->usuario['telefone']}}</span><br/>
                                                <span>Email: {{$result->usuario['email']}}</span>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>

                    <div style="margin-top: 0; padding-top: 0">

                        <hr style="margin-top: 0">
                        <h5>Descrição</h5>
                        <p>
                            {{$result->descricaoProduto}}
                        </p>

                        <hr style="margin-top: 0">
                        <h5>Defeito</h5>
                        <p>
                            {{$result->defeito}}
                        </p>

                        <hr style="margin-top: 0">
                        <h5>Laudo Técnico</h5>
                        <p>
                            {{$result->laudoTecnico}}
                        </p>

                        <hr style="margin-top: 0">
                        <h5>Observações</h5>
                        <p>
                            {{$result->observacoes}}
                        </p>
                        <br />
                        <table class="table table-bordered" id="tblProdutos">
                            <thead>
                                <tr>
                                    <th>Produto</th>
                                    <th>Quantidade</th>
                                    <th>Sub-total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalProdutos = 0; ?>
                                @foreach($produtos as $produto)
                                <?php $totalProdutos = $totalProdutos + $produto->subTotal; ?>
                                <tr>
                                    <th>{{$produto->produto['nome']}}</th>
                                    <th>{{$produto->quantidade}}</th>
                                    <th>R$ {{number_format($produto->subTotal,2,',','.' )}}</th>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ {{number_format($totalProdutos,2,',','.')}}</strong></td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Serviço</th>
                                    <th>Sub-total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalServicos = 0; ?>
                                @foreach($result->servicos as $servico)
                                <?php $totalServicos = $totalServicos + $servico->subTotal; ?>
                                <tr>
                                    <th>{{$servico->servico['nome']}}</th>
                                    <th>R$ {{number_format($servico->subTotal, 2, ',', '.')}}</th>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="1" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ {{number_format($totalServicos, 2, ',', '.')}}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                        <hr />
                        <h4 style="text-align: right">Valor Total: R$ {{number_format($totalServicos + $totalProdutos  ,2,',','.')}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#imprimir").click(function () {
            PrintElem('#printOs');
        })

        function PrintElem(elem)
        {
            Popup($(elem).html());
        }

        function Popup(data)
        {
            var mywindow = window.open('', 'MapOs', 'height=600,width=800');
            mywindow.document.write('<html><head><title>Map Os</title>');
            mywindow.document.write("<link rel='stylesheet' href='{{url('assets/css/bootstrap.min.css')}} />");
            mywindow.document.write("<link rel='stylesheet' href='{{url('assets/css/bootstrap-responsive.min.css')}} />");
            mywindow.document.write("<link rel='stylesheet' href='{{url('assets/css/matrix-style.css')}} />");
            mywindow.document.write("<link rel='stylesheet' href='{{url('assets/css/matrix-media.css')}} />");
            mywindow.document.write("</head><body >");
            mywindow.document.write(data);
            mywindow.document.write("</body></html>");
            mywindow.print();
            mywindow.close();
            return true;
        }
    });
</script>
@endsection
