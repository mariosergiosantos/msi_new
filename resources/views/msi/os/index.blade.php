@extends('tema.topo')
@section('conteudo')
<a href="{{url('os/create')}}" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar OS</a>

<div class="widget-box">
    @include('errors.mensagem')
    <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
        </span>
        <h5>Ordens de Serviço</h5>

    </div>

    <div class="widget-content nopadding">
        <table class="table table-bordered ">
            <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Cliente</th>
                    <th>Data Inicial</th>
                    <th>Data Final</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                @foreach($results as $os)
                <tr>
                    <th>{{$os->cliente['nome']}}</th>
                    <th>{{date('d/m/Y', strtotime($os->dataInicial))}}</th>
                    <th>{{date('d/m/Y', strtotime($os->dataFinal))}}</th>
                    <th>{{$os->status}}</th>
                    <th>
                        <a style="margin-right: 1%" href="{{url('os')}}/{{$os->idOs}}" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>
                        <a style="margin-right: 1%" href="{{url('os')}}/{{$os->idOs}}/edit" class="btn btn-info tip-top" title="Editar OS"><i class="icon-pencil icon-white"></i></a>
                        <!--<a href="#modal-excluir" role="button" data-toggle="modal" os="{{$os->idOs}}" class="btn btn-danger tip-top" title="Excluir OS"><i class="icon-remove icon-white"></i></a>-->
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="col-lg-offset-5">
            {!! $results->render() !!}	
        </div>
    </div>
</div>

<!-- Modal -->
<!--<div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="frm" name="frm">
                <input name="_method" type="hidden" value="DELETE">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 id="myModalLabel">Excluir OS</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idOs" name="id" value="" />
                    <h5 style="text-align: center">Deseja realmente excluir esta OS?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on('click', 'a', function (event) {
                    var os = $(this).attr('os');
                    $('#idOs').val(os);
                    $("#frm").attr("action", "{{url('os')}}/" + os);
                });
            });

        </script>-->
@endsection
