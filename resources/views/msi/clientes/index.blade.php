@extends('tema.topo')
@section('conteudo')
<div class="span12" style="margin-left: 0">
    <form method="get" action="{{url('clientes')}}">
        <div class="span3">
            <a href="{{url('clientes/create')}}" class="btn btn-success span12"><i class="icon-plus icon-white"></i> Adicionar Cliente</a>
        </div>  
        <div class="span5">
            <input type="text" name="search" id="pesquisa"  placeholder="Digite algo para pesquisar" class="span12 form-control" value="{{$request->get('search')}}" >    
        </div>        
        <div class="span1">
            <button class="span12 btn"> <i class="icon-search"></i> </button>
        </div>
        <div class="span1">
            <a href="{{url('relatorios/clientes?acao=imprimir')}}@if($request->get('search'))&search={{$request->get('search')}}@endif" class="span12 btn"> <i class="icon-print icon-white"></i></a>
        </div>
    </form>
</div>
<div class="span12" style="margin-left: 0">
    <div class="widget-box">
        @include('errors.mensagem')
        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Clientes</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF/CNPJ</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($clientes) <1 )
                    <tr>
                        <td colspan="4">Nenhum cliente encontrado</td>
                    </tr>
                    @endif

                    @foreach($clientes as $cliente)
                    <tr>
                        <th>{{$cliente->nome}}</th>
                        <th>{{$cliente->documento}}</th>
                        <th>{{$cliente->telefone}}</th>
                        <th>{{$cliente->email}}</th>
                        <th>
                            <a style="margin-right: 1%" href="{{url('clientes')}}/{{$cliente->idClientes}}" class="btn tip-top" title="Visualizar Cliente"><i class="icon-eye-open"></i></a> 
                            <a style="margin-right: 1%" href="{{url('clientes')}}/{{$cliente->idClientes}}/edit" class="btn btn-info tip-top" title="Editar Cliente"><i class="icon-pencil icon-white"></i></a>
                            <a href="#modal-excluir" role="button" data-target="#modal-excluir" data-toggle="modal" cliente="{{$cliente->idClientes}}" class="btn btn-danger tip-top" title="Excluir Cliente"><i class="icon-remove icon-white"></i></a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-offset-5">
                {!! $clientes->render() !!}	
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="post" id="frm" name="frm">
                    <input name="_method" type="hidden" value="DELETE">
                    {!! csrf_field() !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Cliente</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="idCliente" name="id" value="" />
                        <h5 style="text-align: center">Deseja realmente excluir este cliente e os dados associados a ele (OS, Vendas, Receitas)?</h5>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button class="btn btn-danger">Excluir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var cliente = $(this).attr('cliente');
            $('#idCliente').val(cliente);
            $("#frm").attr("action", "{{url('clientes')}}/" + cliente);
        });
    });
</script>
@endsection
