@extends('tema.topo')
@section('conteudo')
<div class="widget-box">
    <div class="widget-title">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab1">Dados do Cliente</a></li>
            <li><a data-toggle="tab" href="#tab2">Compras</a></li>
            <div class="buttons">
                <a title="Icon Title" class="btn btn-mini btn-info" href="{{url('clientes')}}/{{$result->idClientes}}/edit"><i class="icon-pencil icon-white"></i> Editar</a>
            </div>
        </ul>
    </div>
    <div class="widget-content tab-content">
        <div id="tab1" class="tab-pane active" style="min-height: 300px">

            <div class="accordion" id="collapse-group">
                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span><h5>Dados Pessoais</h5>
                            </a>
                        </div>
                    </div>
                    <div class="collapse in accordion-body" id="collapseGOne">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Nome</strong></td>
                                        <td>{{$result->nome}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Documento</strong></td>
                                        <td>{{$result->documento}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Data de Cadastro</strong></td>
                                        <td>{{date('d/m/Y', strtotime($result->created_at))}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span><h5>Contatos</h5>
                            </a>
                        </div>
                    </div>
                    <div class="collapse accordion-body" id="collapseGTwo">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Telefone</strong></td>
                                        <td>{{$result->telefone}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Celular</strong></td>
                                        <td>{{$result->celular}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Email</strong></td>
                                        <td>{{$result->email}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(!$result->endereco->isEmpty())
                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span><h5>Endereço principal</h5>
                            </a>
                        </div>
                    </div>                    
                    <div class="collapse accordion-body" id="collapseGThree">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Rua</strong></td>
                                        <td>{{($result->endereco) != null ? $result->endereco[0]->rua : ""}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Número</strong></td>
                                        <td>{{($result->endereco) != null ? $result->endereco[0]->numero : ""}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Bairro</strong></td>
                                        <td>{{($result->endereco) != null ? $result->endereco[0]->bairro : ""}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Cidade</strong></td>
                                        <td>{{($result->endereco) != null ? $result->endereco[0]->cidade : ""}} - {{($result->endereco) != null ? $result->endereco[0]->estado : ""}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>CEP</strong></td>
                                        <td>{{($result->endereco) != null ? $result->endereco[0]->cep : ""}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <!--Tab 2-->
        <div id="tab2" class="tab-pane" style="min-height: 300px">

            <table class="table table-bordered ">
                <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th><strong>Data da compra</strong></th>
                        <th><strong>Quantidade de produtos</strong></th>
                        <th><strong>Valor total</strong></th>
                        <th>Pago</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    @for($i = 0; $i < count($result['vendas']); $i++)

                    <?php
                    $qtdProdutos = 0;
                    $subTotal = 0;
                    ?>
                    @for($x = 0; $x < count($result['itens']); $x++)
                    <?php
                    $qtdProdutos += $result->itens[$x]->quantidade;
                    $subTotal += $result->itens[$x]->subTotal;
                    ?>
                    @endfor

                    <tr>
                        <th>{{date('d/m/Y', strtotime($result->vendas[$i]->created_at))}}</th>
                        <th>{{$qtdProdutos}}</th>
                        <th>R$ {{number_format($subTotal, 2, ',', '.')}} </th>
                        <th>
                            @if($result->faturado == 1)
                            Sim
                            @else
                            Não
                            @endif
                        </th>
                        <th>
                            <a style="margin-right: 1%" href="{{url('vendas')}}/{{$result->vendas[$i]->idVendas}}" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>
                        </th>
                    </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection