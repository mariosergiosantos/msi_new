@extends('tema.topo')
@section('conteudo')
    <div class="row-fluid" style="margin-top:0">
        <div class="col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                    <h5>Editar Dados do Emitente</h5>
                </div>
                <div class="widget-content nopadding">
                    <form action="{{url('emitente')}}" id="formAlterar" method="post" class="form-horizontal">
                    <input name="_method" type="hidden" value="PUT">
                    <input id="nome" type="text" name="nome" value="<?php echo $result->nome; ?>"  />
                        <input id="nome" type="hidden" name="id" value="<?php echo $result->id; ?>"  />
                        {!! csrf_field() !!}<br>

                        <div class="modal-body">
                <div class="form-group">
                    <label for="cnpj" class="control-label col-md-2"><span class="required">CNPJ*</span></label>
                    <div class="col-md-4">
                        <input class="" type="text" name="cnpj" id="cnpj" value="<?php echo $result->cnpj; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">IE*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="ie" value="<?php echo $result->ie; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">Logradouro*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="logradouro" value="<?php echo $result->rua; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">Número*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="numero" value="<?php echo $result->numero; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">Bairro*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="bairro" value="<?php echo $result->bairro; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">Cidade*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="cidade" value="<?php echo $result->cidade; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">UF*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="uf" value="<?php echo $result->uf; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">Telefone*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="telefone" value="<?php echo $result->telefone; ?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="descricao" class="control-label col-md-2"><span class="required">E-mail*</span></label>
                    <div class="col-md-4">
                        <input type="text" name="email" value="<?php echo $result->email; ?>" />
                    </div>
                </div>
            </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4 col-md-offset-2">
                                    <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i>
                                        Adicionar
                                    </button>
                                    <a href="{{url('emitente')}}" id="" class="btn"><i class="icon-arrow-left"></i>
                                        Voltar</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{url('assets/js/jquery.mask.js')}}"></script>
    <script src="{{url('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var telefone = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            }, spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(telefone.apply({}, arguments), options);
                }
            };
            $('.telefone').mask(telefone, spOptions);
            //Documento
            $("#cnpj").keydown(function () {
                  $("#cnpj").mask("99.999.999/9999-99");
            }
            
            $("#formAlterar").validate({
            rules: {
                userfile: {required: true},
                nome: {required: true},
                cnpj: {required: true},
                ie: {required: true},
                logradouro: {required: true},
                numero: {required: true},
                bairro: {required: true},
                cidade: {required: true},
                uf: {required: true},
                telefone: {required: true},
                email: {required: true}
            },
            messages: {
                userfile: {required: 'Campo Requerido.'},
                nome: {required: 'Campo Requerido.'},
                cnpj: {required: 'Campo Requerido.'},
                ie: {required: 'Campo Requerido.'},
                logradouro: {required: 'Campo Requerido.'},
                numero: {required: 'Campo Requerido.'},
                bairro: {required: 'Campo Requerido.'},
                cidade: {required: 'Campo Requerido.'},
                uf: {required: 'Campo Requerido.'},
                telefone: {required: 'Campo Requerido.'},
                email: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
            
        });
        
        
    </script>
@endsection
