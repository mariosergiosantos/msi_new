@extends('tema.topo')
@section('conteudo')
<?php if (!isset($results) || $results == null) { ?>
    <div class="row-fluid" style="margin-top:0">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>
                    </span>
                    <h5>Dados do Emitente</h5>
                </div>
                <div class="widget-content ">
                    <div class="alert alert-danger">Nenhum dado foi cadastrado até o momento. Essas informações 
                        estarão disponíveis na tela de impressão de OS.</div>
                    <a href="{{url('emitente/create')}}" data-toggle="modal" role="button" class="btn btn-success">Cadastrar Dados</a>
                </div>
            </div>    
        </div>
    </div>

<?php } else { ?>

    <div class="row-fluid" style="margin-top:0">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-align-justify"></i>
                    </span>
                    <h5>Dados do Emitente</h5>
                </div>
                <div class="widget-content ">
                     @include('errors.mensagem')
                    <div class="alert alert-info">Os dados abaixo serão utilizados no cabeçalho das telas de impressão.</div>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style="width: 25%"><img src=" <?php echo $results[0]->url_logo; ?> "></td>
                                <td> <span style="font-size: 20px; "> <?php echo $results[0]->nome; ?> </span> </br><span><?php echo $results[0]->cnpj; ?> </br> <?php echo $results[0]->rua . ', nº:' . $results[0]->numero . ', ' . $results[0]->bairro . ' - ' . $results[0]->cidade . ' - ' . $results[0]->uf; ?> </span> </br> <span> E-mail: <?php echo $results[0]->email . ' - Fone: ' . $results[0]->telefone; ?></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{url('emitente')}}/{{$results[0]->id}}/edit" data-toggle="modal" role="button" class="btn btn-primary">Alterar Dados</a>
                    <a href="#modalLogo" data-toggle="modal" role="button" class="btn btn-inverse">Alterar Logo</a>
                </div>
            </div>
        </div>
    </div>

    <div id="modalLogo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="{{url('editarLogo')}}" id="formLogo" enctype="multipart/form-data" method="post" class="form-horizontal" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="">MapOS - Alterar Logomarca</h3>
            </div>
            <div class="modal-body">
                <div class="span12 alert alert-info">Selecione uma nova imagem da logomarca. Tamanho indicado (130 X 130).</div>          
                <div class="control-group">
                    <label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
                    <div class="controls">
                        <input type="file" name="userfile" value="" />
                        <input id="nome" type="hidden" name="id" value="<?php echo $results[0]->id; ?>"  />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
                <button class="btn btn-primary">Alterar</button>
            </div>
        </form>
    </div>
<?php } ?>
<script type="text/javascript" src="{{url('js/jquery.validate.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#formLogo").validate({
            rules: {
                userfile: {required: true}
            },
            messages: {
                userfile: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>
@endsection
