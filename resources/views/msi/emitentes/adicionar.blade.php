@extends('tema.topo')
@section('conteudo')
    <div class="row-fluid" style="margin-top:0">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                    <h5>Cadastrar Dados do Emitente</h5>
                </div>
                <div class="widget-content nopadding">
                    <form action="{{url('emitentes')}}" id="formCadastrar" method="post" class="form-horizontal">
                        {!! csrf_field() !!}<br>

                        <div class="modal-body">
                <div class="control-group">
                    <label for="nome" class="control-label">Razão Social<span class="required">*</span></label>
                    <div class="controls">
                        <input id="nome" type="text" name="nome" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="cnpj" class="control-label"><span class="required">CNPJ*</span></label>
                    <div class="controls">
                        <input class="" type="text" name="cnpj" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">IE*</span></label>
                    <div class="controls">
                        <input type="text" name="ie" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">Logradouro*</span></label>
                    <div class="controls">
                        <input type="text" name="logradouro" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">Número*</span></label>
                    <div class="controls">
                        <input type="text" name="numero" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">Bairro*</span></label>
                    <div class="controls">
                        <input type="text" name="bairro" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">Cidade*</span></label>
                    <div class="controls">
                        <input type="text" name="cidade" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">UF*</span></label>
                    <div class="controls">
                        <input type="text" name="uf" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">Telefone*</span></label>
                    <div class="controls">
                        <input type="text" name="telefone" value=""  />
                    </div>
                </div>
                <div class="control-group">
                    <label for="descricao" class="control-label"><span class="required">E-mail*</span></label>
                    <div class="controls">
                        <input type="text" name="email" value="" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
                    <div class="controls">
                        <input type="file" name="userfile" value="" />
                    </div>
                </div>
            </div>
                        <div class="form-group">
                            <div class="span12">
                                <div class="col-md-4 col-md-offset-2">
                                    <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i>
                                        Adicionar
                                    </button>
                                    <a href="{{url('emitente')}}" id="" class="btn"><i class="icon-arrow-left"></i>
                                        Voltar</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{url('assets/js/jquery.mask.js')}}"></script>
    <script src="{{url('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var telefone = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            }, spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(telefone.apply({}, arguments), options);
                }
            };
            $('.telefone').mask(telefone, spOptions);
            //Documento
            $("#cnpj").keydown(function () {
                  $("#cnpj").mask("99.999.999/9999-99");
            }
            
            $("#formCadastrar").validate({
            rules: {
                userfile: {required: true},
                nome: {required: true},
                cnpj: {required: true},
                ie: {required: true},
                logradouro: {required: true},
                numero: {required: true},
                bairro: {required: true},
                cidade: {required: true},
                uf: {required: true},
                telefone: {required: true},
                email: {required: true}
            },
            messages: {
                userfile: {required: 'Campo Requerido.'},
                nome: {required: 'Campo Requerido.'},
                cnpj: {required: 'Campo Requerido.'},
                ie: {required: 'Campo Requerido.'},
                logradouro: {required: 'Campo Requerido.'},
                numero: {required: 'Campo Requerido.'},
                bairro: {required: 'Campo Requerido.'},
                cidade: {required: 'Campo Requerido.'},
                uf: {required: 'Campo Requerido.'},
                telefone: {required: 'Campo Requerido.'},
                email: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
            
        });
    </script>
@endsection
