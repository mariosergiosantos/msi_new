<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Map OS</title>
        <meta charset="UTF-8" />
        @if(request()->user())
        <meta http-equiv="refresh" content="600; url={{url('auth/logout')}}" />
        @endif
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{url('assets/css/bootstrap-responsive.min.css')}}" />
        <link rel="stylesheet" href="{{url('assets/css/matrix-style.css')}}" />
        <link rel="stylesheet" href="{{url('assets/css/matrix-media.css')}}" />
        <link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{url('assets/css/fullcalendar.css')}}" /> 
        <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
        <script type="text/javascript"  src="{{url('assets/js/jquery-1.10.2.min.js')}}"></script>
    </head>
    <body>
        <div id="header">
            <h1><a href="">Map OS</a></h1>
        </div>
        <div id="user-nav">
            <ul class="nav">
                <li class=""><a title="Minha conta" href="{{url('usuarios')}}/{{ request()->user()->idUsuarios }}"><i class="icon icon-star"></i> <span class="text">Minha Conta</span></a></li>
                <li class=""><a title="Sair" href="{{url('auth/logout')}}"><i class="icon icon-share-alt"></i> <span class="text">Sair do Sistema</span></a></li>
            </ul>
        </div>
        <div id="search">
            <form action="{{url('search')}}">
                <input type="text" name="q" placeholder="Pesquisar..." value="{{request()->get('q')}}"/>
                <button type="submit"  class="tip-bottom" title="Pesquisar"><i class="icon-search icon-white"></i></button>
            </form>
        </div>
        <?php
        $paths = explode("/", request()->path());
        ?>
        <div id="sidebar"> <a href="#" class="visible-phone"><i class="icon icon-list"></i> Menu</a>
            <ul>
                <li class="@if($paths[0] == 'dashboard') active @endif"><a href="{{url('')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
                <li class="@if($paths[0] == 'clientes') active @endif"><a href="{{url('clientes')}}"><i class="icon icon-group"></i> <span>Clientes</span></a></li>
                <li class="@if($paths[0] == 'fornecedores') active @endif"><a href="{{url('fornecedores')}}"><i class="icon icon-group"></i> <span>Fornecedores</span></a></li>
                <li class="@if($paths[0] == 'produtos') active @endif"><a href="{{url('produtos')}}"><i class="icon icon-barcode"></i> <span>Produtos</span></a></li>
                <!--<li class="@if($paths[0] == 'servicos') active @endif"><a href="{{url('servicos')}}"><i class="icon icon-wrench"></i> <span>Serviços</span></a></li>-->
                <!--<li class="@if($paths[0] == 'os') active @endif"><a href="{{url('os')}}"><i class="icon icon-tags"></i> <span>Ordens de Serviço</span></a></li>-->
                <!--<li class="@if($paths[0] == 'vendas') active @endif"><a href="{{url('vendas')}}"><i class="icon icon-shopping-cart"></i> <span>Vendas</span></a></li>-->
                <li class="submenu @if($paths[0] == 'compras') active @endif">
                    <a href="{{url('compras')}}"><i class="icon icon-money"></i> <span>Compras</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
                    <ul>
                        <li><a href="{{url('compras')}}">Pedidos</a></li>
                        <li><a href="{{url('compras/orcamento')}}">Orçamento</a></li>
                        <li><a href="{{url('compras/tipo-pagamento')}}">Tipo de pagamento</a></li>
                    </ul>
                </li>
                <li class="submenu @if($paths[0] == 'vendas') active @endif">
                    <a href="{{url('vendas')}}"><i class="icon icon-money"></i> <span>Vendas</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
                    <ul>
                        <li><a href="{{url('vendas')}}">Pedidos</a></li>
                        <li><a href="{{url('vendas/orcamento')}}">Orçamento</a></li>
                        <li><a href="{{url('vendas/tipo-pagamento')}}">Tipo de pagamento</a></li>
                    </ul>
                </li>
                <li class="@if($paths[0] == 'arquivos') active @endif"><a href="{{url('arquivos')}}"><i class="icon icon-hdd"></i> <span>Arquivos</span></a></li>
                <li class="submenu @if($paths[0] == 'lancamentos') active @endif">
                    <a href="#"><i class="icon icon-money"></i> <span>Financeiro</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
                    <ul>
                        <li><a href="{{url('lancamentos')}}">Lançamentos</a></li>
                    </ul>
                </li>
                <li class="submenu @if($paths[0] == 'relatorios') active @endif">
                    <a href="#"><i class="icon icon-list-alt"></i> <span>Relatórios</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
                    <ul>
                        <li><a href="{{url('relatorios/clientes')}}" >Clientes</a></li>
                        <li><a href="{{url('relatorios/produtos')}}">Produtos</a></li>
                        <!--<li><a href="{{url('relatorios/servicos')}}">Serviços</a></li>
                        <li><a href="{{url('relatorios/os')}}">Ordens de Serviço</a></li>-->
                        <li><a href="{{url('relatorios/vendas')}}">Vendas</a></li>
                        <li><a href="{{url('relatorios/financeiro')}}">Financeiro</a></li>
                    </ul>
                </li>
                <li class="submenu @if($paths[0] == 'configuracoes') active @endif">
                    <a href="#"><i class="icon icon-cog"></i> <span>Configurações</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
                    <ul>
                        <li><a href="{{url('usuarios')}}">Usuários</a></li>
                        <li><a href="{{url('emitente')}}">Emitente</a></li>
                        <li><a href="{{url('permissoes')}}">Permissões</a></li>
                        <li><a href="{{url('backup')}}">Backup</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div id="content">
            <div id="content-header">
                <div id="breadcrumb">
                    <a href="{{url('dashboard')}}" class="tip-bottom"><i class="icon-home"></i> Dashboard</a>
                    <a href="{{url($paths[0])}}" class="tip-bottom" title="{{ucfirst($paths[0])}}">{{ucfirst($paths[0])}} </a>
                    @if(count($paths) > 1)
                    
                        @if(end($paths) == 'create')
                            <a href="#" class="current tip-bottom" title="Adicionar"> Adicionar</a>
                        @endif
                        
                        @if(end($paths) == 'edit')
                            <a href="#" class="current tip-bottom" title="Editar"> Editar</a>
                        @endif
                        
                        @if(is_numeric(end($paths)))
                            <a href="#" class="current tip-bottom" title="Visualizar"> Visualizar</a>
                        @endif
                    
                    @endif
                </div>
            </div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="col-md-12">
                        @yield('conteudo')
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div id="footer" class="col-md-12"> 2014 &copy; MAP OS</div>
        </div>
        <script src="{{url('assets/js/main.js')}}"></script>
        <script type="text/javascript" src="{{url('assets/js/bootstrap-filestyle.min.js')}}"></script>
        <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
        <!--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
        <script src="{{url('assets/js/matrix.js')}}"></script> 
    </body>
</html>
