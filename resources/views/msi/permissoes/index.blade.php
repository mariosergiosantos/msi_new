@extends('tema.topo')
@section('conteudo')
<a href="{{url('permissoes/create')}}" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Permissão</a>
<?php
if(!$results){?>
        <div class="widget-box">
         @include('errors.mensagem')
        <div class="widget-title">
            <span class="icon">
                <i class="icon-lock"></i>
            </span>
            <h5>Permissões</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Data de Criação</th>
                        <th>Situação</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">Nenhuma Permissão foi cadastrada</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php }

else{?>

<div class="widget-box">
     <div class="widget-title">
        <span class="icon">
            <i class="icon-lock"></i>
         </span>
        <h5>Permissões</h5>
     </div>

<div class="widget-content nopadding">

<table class="table table-bordered ">
    <thead>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Data de Criação</th>
            <th>Situação</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($results as $r) {
            if($r->situacao == 1){$situacao = 'Ativo';}else{$situacao = 'Inativo';}
            echo '<tr>';
            echo '<td>'.$r->idPermissao.'</td>';
            echo '<td>'.$r->nome.'</td>';
            echo '<td>'.date('d/m/Y',strtotime($r->data)).'</td>';
            echo '<td>'.$situacao.'</td>';
            echo '<td>
                      <a href="permissoes/' . $r->idPermissao . '/edit" class="btn btn-info tip-top" title="Editar Permissão"><i class="icon-pencil icon-white"></i></a>
                      <a href="#modal-excluir" role="button" data-toggle="modal" permissao="'.$r->idPermissao.'" class="btn btn-danger tip-top" title="Desativar Permissão"><i class="icon-remove icon-white"></i></a>
                  </td>';
            echo '</tr>';
        }?>
        <tr>
            
        </tr>
    </tbody>
</table>
<div class="col-lg-offset-5">
            {!! $results->render() !!}	
        </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
  <form action="" method="post" id="frm" >
  	 <input name="_method" type="hidden" value="DELETE">
                {!! csrf_field() !!}
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Desativar Permissão</h5>
  </div>
  <div class="modal-body">
    <h5 style="text-align: center">Deseja realmente desativar esta permissão?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
   $(document).on('click', 'a', function(event) {
        var permissao = $(this).attr('permissao');
        $('#idPermissao').val(permissao);
        $("#frm").attr("action", "{{url('permissoes')}}/" + permissao);
    });
});
</script>	
<?php	
}
?>
@endsection
