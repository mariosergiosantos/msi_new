@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-lock"></i>
                </span>
                <h5>Cadastro de Permissão</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('permissoes')}}" id="formPermissao" method="post" class="form-horizontal">
                    {!! csrf_field() !!}<br>
                    <div class="col-md-6">
                        <label>Nome da Permissão</label>
                        <input name="nome" type="text" id="nome" class="form-control" />
                    </div>

                    <div class="col-md-6">
                        <br/><br>
                        <label>
                            <input name="marcarTodos" type="checkbox" value="1" id="marcarTodos" />
                            <span class="lbl"> Marcar Todos</span>
                        </label>
                        <br/>
                    </div>

                    <div class="col-md-12">
                        <label for="documento" class="control-label"></label>
                        <div class="form-group">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                <input name="vCliente" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar Cliente</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aCliente" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar Cliente</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eCliente" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar Cliente</span>
                                            </label>
                                        </td>
                                        <td>
                                            <label>
                                                <input name="dCliente" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir Cliente</span>
                                            </label>
                                        </td>
                                    </tr>

                                    <tr><td colspan="4"></td></tr>
                                    <tr>
                                        <td>
                                            <label>
                                                <input name="vProduto" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar Produto</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aProduto" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar Produto</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eProduto" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar Produto</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="dProduto" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir Produto</span>
                                            </label>
                                        </td>

                                    </tr>
                                    <tr><td colspan="4"></td></tr>

                                    <tr>

                                        <td>
                                            <label>
                                                <input name="vServico" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar Serviço</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aServico" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar Serviço</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eServico" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar Serviço</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="dServico" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir Serviço</span>
                                            </label>
                                        </td>

                                    </tr>

                                    <tr><td colspan="4"></td></tr>
                                    <tr>

                                        <td>
                                            <label>
                                                <input name="vOs" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar OS</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aOs" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar OS</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eOs" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar OS</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="dOs" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir OS</span>
                                            </label>
                                        </td>

                                    </tr>
                                    <tr><td colspan="4"></td></tr>

                                    <tr>

                                        <td>
                                            <label>
                                                <input name="vVenda" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar Venda</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aVenda" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar Venda</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eVenda" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar Venda</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="dVenda" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir Venda</span>
                                            </label>
                                        </td>

                                    </tr>

                                    <tr><td colspan="4"></td></tr>

                                    <tr>

                                        <td>
                                            <label>
                                                <input name="vArquivo" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar Arquivo</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aArquivo" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar Arquivo</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eArquivo" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar Arquivo</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="dArquivo" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir Arquivo</span>
                                            </label>
                                        </td>
                                    </tr>

                                    <tr><td colspan="4"></td></tr>

                                    <tr>

                                        <td>
                                            <label>
                                                <input name="vLancamento" class="marcar" type="checkbox" checked="checked" value="1" />
                                                <span class="lbl"> Visualizar Lançamento</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="aLancamento" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Adicionar Lançamento</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="eLancamento" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Editar Lançamento</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="dLancamento" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Excluir Lançamento</span>
                                            </label>
                                        </td>

                                    </tr>

                                    <tr><td colspan="4"></td></tr>

                                    <tr>
                                        <td>
                                            <label>
                                                <input name="rCliente" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Relatório Cliente</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="rServico" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Relatório Serviço</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="rOs" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Relatório OS</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="rProduto" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Relatório Produto</span>
                                            </label>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td>
                                            <label>
                                                <input name="rVenda" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Relatório Venda</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="rFinanceiro" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Relatório Financeiro</span>
                                            </label>
                                        </td>
                                        <td colspan="2"></td>

                                    </tr>
                                    <tr><td colspan="4"></td></tr>

                                    <tr>
                                        <td>
                                            <label>
                                                <input name="cUsuario" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Configurar Usuário</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="cEmitente" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Configurar Emitente</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="cPermissao" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Configurar Permissão</span>
                                            </label>
                                        </td>

                                        <td>
                                            <label>
                                                <input name="cBackup" class="marcar" type="checkbox" value="1" />
                                                <span class="lbl"> Backup</span>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="{{url('permissoes')}}" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript" src="{{url('assets/js/validate.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {

    $(document).on('click', '#marcarTodos', function (event) {
        if ($(this).prop('checked')) {
            $('.marcar').each(
                    function () {
                        $(this).attr("checked", true);
                    }
            );
        } else {
            $('.marcar').each(
                    function () {
                        $(this).attr("checked", false);
                    }
            );
        }
    });

    $("#formPermissao").validate({
        rules: {
            nome: {required: true}
        },
        messages: {
            nome: {required: 'Campo obrigatório'}
        }
    });
});
</script>
@endsection
