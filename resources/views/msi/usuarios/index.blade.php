@extends('tema.topo')
@section('conteudo')
<a href="{{url('usuarios/create')}}" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Usuário</a>
<?php if (!$results) { ?>
    <div class="widget-box">
        @include('errors.mensagem')
        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Usuários</h5>

        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th>#</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Telefone</th>
                        <th>Nível</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>    
                    <tr>
                        <td colspan="5">Nenhum Usuário Cadastrado</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Usuários</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th>#</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Telefone</th>
                        <th>Nível</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $r)
                    <tr>
                        <th>#{{$r->idUsuarios}}</th>
                        <th>{{$r->nome}}</th>
                        <th>{{$r->cpf}}</th>
                        <th>{{$r->telefone}}</th>
                        <th>{{$r->permissao}}</th>
                        <th>
                            <a style="margin-right: 1%" href="{{url('usuarios')}}/{{$r->idUsuarios}}" class="btn tip-top" title="Visualizar Usuário"><i class="icon-eye-open"></i></a>
                            <a style="margin-right: 1%" href="{{url('usuarios')}}/{{$r->idUsuarios}}/edit" class="btn btn-info tip-top" title="Editar Usuário"><i class="icon-pencil icon-white"></i></a>
                            <a href="#modal-excluir" role="button" data-target="#modal-excluir" data-toggle="modal" usuario="{{$r->idUsuarios}}" class="btn btn-danger tip-top" title="Excluir Usuário"><i class="icon-remove icon-white"></i></a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-offset-5">
                {!! $results->render() !!}	
            </div>
        </div>
    </div>
<?php } ?>
@endsection
