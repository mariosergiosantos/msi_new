@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Cadastro de Usuário</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('usuarios')}}>" id="formUsuario" method="post" class="form-horizontal" >
                        {!! csrf_field() !!}<br>
                        
                    <div class="form-group">
                        <label for="nome" class="control-label col-md-2">Nome<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="nome" class="form-control" type="text" name="nome" value="{{old('nome')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="rg" class="control-label col-md-2">RG<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="rg" class="form-control" type="text" name="rg" value="{{old('rg')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cpf" class="control-label col-md-2">CPF<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="cpf" class="form-control" type="text" name="cpf" value="{{old('cpf')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="rua" class="control-label col-md-2">Rua<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="rua" class="form-control" type="text" name="rua" value="{{old('rua')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="numero" class="control-label col-md-2">Numero<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="numero" class="form-control" type="text" name="numero" value="{{old('numero')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="bairro" class="control-label col-md-2">Bairro<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="bairro" class="form-control" type="text" name="bairro" value="{{old('bairro')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cidade" class="control-label col-md-2">Cidade<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="cidade" class="form-control" type="text" name="cidade" value="{{old('cidade')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="estado" class="control-label col-md-2">Estado<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="estado" class="form-control" type="text" name="estado" value="{{old('estado')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="control-label col-md-2">Email<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="email" class="form-control" type="text" name="email" value="{{old('email')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="senha" class="control-label col-md-2">Senha<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="senha" class="form-control" type="password" name="senha" value="{{old('senha')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="telefone" class="control-label col-md-2">Telefone<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="telefone" class="form-control" type="text" name="telefone" value="{{old('telefone')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="celular" class="control-label col-md-2">Celular</label>
                        <div class="col-md-4">
                            <input id="celular" class="form-control" type="text" name="celular" value="{{old('celular')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="control-label col-md-2">Situação*</label>
                        <div class="col-md-4">
                            <select name="situacao" class="form-control" id="situacao">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="control-label col-md-2">Perfil<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="role_id" class="form-control" id="role_id">
                                  @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                  @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="{{url('usuarios')}}" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script  src="{{url('js/jquery.validate.js')}}"></script>
<script type="text/javascript">
      $(document).ready(function(){

           $('#formUsuario').validate({
            rules : {
                  nome:{ required: true},
                  rg:{ required: true},
                  cpf:{ required: true},
                  telefone:{ required: true},
                  email:{ required: true},
                  senha:{ required: true},
                  rua:{ required: true},
                  numero:{ required: true},
                  bairro:{ required: true},
                  cidade:{ required: true},
                  estado:{ required: true},
                  cep:{ required: true}
            },
            messages: {
                  nome :{ required: 'Campo Requerido.'},
                  rg:{ required: 'Campo Requerido.'},
                  cpf:{ required: 'Campo Requerido.'},
                  telefone:{ required: 'Campo Requerido.'},
                  email:{ required: 'Campo Requerido.'},
                  senha:{ required: 'Campo Requerido.'},
                  rua:{ required: 'Campo Requerido.'},
                  numero:{ required: 'Campo Requerido.'},
                  bairro:{ required: 'Campo Requerido.'},
                  cidade:{ required: 'Campo Requerido.'},
                  estado:{ required: 'Campo Requerido.'},
                  cep:{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
      });
</script>
@endsection
