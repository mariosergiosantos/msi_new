@extends('tema.topo')
@section('conteudo')
<a href="{{url('fornecedores/create')}}" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Fornecedor</a>    
<div class="widget-box">
    @include('errors.mensagem')
    <div class="widget-title">
        <span class="icon">
            <i class="icon-user"></i>
        </span>
        <h5>Fornecedores</h5>
    </div>

    <div class="widget-content nopadding">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>CNPJ</th>
                    <th>Telefone</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $fornecedor)
                <tr>
                    <th>{{$fornecedor->nome}}</th>
                    <th>{{$fornecedor->cnpj}}</th>
                    <th>{{$fornecedor->telefone}}</th>
                    <th>
                        <a style="margin-right: 1%" href="{{url('fornecedores')}}/{{$fornecedor->id}}" class="btn tip-top" title="Visualizar Fornecedor"><i class="icon-eye-open"></i></a> 
                        <a style="margin-right: 1%" href="{{url('fornecedores')}}/{{$fornecedor->id}}/edit" class="btn btn-info tip-top" title="Editar Fornecedor"><i class="icon-pencil icon-white"></i></a>
                        <a href="#modal-excluir" role="button" data-target="#modal-excluir" data-toggle="modal" fornecedor="{{$fornecedor->id}}" class="btn btn-danger tip-top" title="Excluir Fornecedor"><i class="icon-remove icon-white"></i></a>
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="col-lg-offset-5">
            {!! $results->render() !!}	
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="frm" name="frm">
                <input name="_method" type="hidden" value="DELETE">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Fornecedor</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idFornecedor" name="id" value="" />
                    <h5 style="text-align: center">Deseja realmente excluir este fornecedor e os dados associados a ele (Produtos)?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var fornecedor = $(this).attr('fornecedor');
            $('#idFornecedor').val(fornecedor);
            $("#frm").attr("action", "{{url('fornecedores')}}/" + fornecedor);
        });
    });
</script>
@endsection
