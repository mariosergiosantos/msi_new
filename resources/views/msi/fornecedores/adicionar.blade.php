@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Cadastro de Fornecedor</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('clientes')}}" id="formFornecedor" method="post" class="form-horizontal">
                    {!! csrf_field() !!}<br>

                    <div class="form-group">
                        <label for="nomeFornecedor" class="control-label col-md-2">Nome<span class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="nomeFornecedor" class="form-control" type="text" name="nomeFornecedor"
                                   value="{{old('nomeFornecedor')}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="documento" class="control-label col-md-2">CPF/CNPJ<span
                                class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="documento" class="form-control" type="text" name="documento"
                                   value="{{old('documento')}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telefone" class="control-label col-md-2">Telefone<span class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="telefone" class="form-control telefone" type="text" name="telefone"
                                   value="{{old('telefone')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="celular" class="control-label col-md-2">Celular</label>

                        <div class="col-md-4">
                            <input id="celular" class="form-control telefone" type="text" name="celular"
                                   value="{{old('celular')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="control-label col-md-2">Email<span
                                class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="email" class="form-control" type="text" name="email"
                                   value="{{old('email')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cep" class="control-label col-md-2">CEP<span class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="cep" class="form-control" type="text" name="cep" value="{{old('cep')}}"/>
                        </div>
                    </div>

                    <div class="form-group" class="control-label">
                        <label for="rua" class="control-label col-md-2">Rua<span class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="rua" class="form-control" type="text" name="rua" value="{{old('rua')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="numero" class="control-label col-md-2">Número<span
                                class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="numero" class="form-control" type="text" name="numero"
                                   value="{{old('numero')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="bairro" class="control-label col-md-2">Bairro<span
                                class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="bairro" class="form-control" type="text" name="bairro"
                                   value="{{old('bairro')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cidade" class="control-label col-md-2">Cidade<span
                                class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="cidade" class="form-control" type="text" name="cidade"
                                   value="{{old('cidade')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="estado" class="control-label col-md-2">Estado<span
                                class="required">*</span></label>

                        <div class="col-md-4">
                            <input id="estado" class="form-control" type="text" name="estado"
                                   value="{{old('estado')}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i>
                                    Adicionar
                                </button>
                                <a href="{{url('clientes')}}" id="" class="btn"><i class="icon-arrow-left"></i>
                                    Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{url('assets/js/jquery.mask.js')}}"></script>
<script src="{{url('js/jquery.validate.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {

    $("#cep").change(function () {
        var cep_code = $(this).val();
        if (cep_code.length <= 0)
            return;
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", {code: cep_code},
        function (result) {
            if (result.status != 1) {
                alert(result.message || "Houve um erro desconhecido");
                return;
            }
            $("#cep").val(result.code);
            $("#estado").val(result.state);
            $("#cidade").val(result.city);
            $("#bairro").val(result.district);
            $("#endereco").val(result.address);
            $("#estado").val(result.state);
        });
    });

    var telefone = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    }, spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(telefone.apply({}, arguments), options);
        }
    };
    $('.telefone').mask(telefone, spOptions);

    //Documento
    $("#documento").keydown(function () {
        try {
            $("#documento").unmask();
        } catch (e) {
        }

        var tamanho = $("#documento").val().length;
        if (tamanho < 11) {
            $("#documento").mask("999.999.999-99");
        } else if (tamanho >= 11) {
            $("#documento").mask("99.999.999/9999-99");
        }
    });


    $('#cep').mask('00000-000');

    $('#formFornecedor').validate({
        rules: {
            nomeFornecedor: {required: true},
            documento: {required: true},
            telefone: {required: true},
            email: {required: true},
            rua: {required: true},
            numero: {required: true},
            bairro: {required: true},
            cidade: {required: true},
            estado: {required: true},
            cep: {required: true}
        },
        messages: {
            nomeFornecedor: {required: 'Campo Requerido.'},
            documento: {required: 'Campo Requerido.'},
            telefone: {required: 'Campo Requerido.'},
            email: {required: 'Campo Requerido.'},
            rua: {required: 'Campo Requerido.'},
            numero: {required: 'Campo Requerido.'},
            bairro: {required: 'Campo Requerido.'},
            cidade: {required: 'Campo Requerido.'},
            estado: {required: 'Campo Requerido.'},
            cep: {required: 'Campo Requerido.'}

        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
});
</script>
@endsection
