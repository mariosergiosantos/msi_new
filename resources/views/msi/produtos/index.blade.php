@extends('tema.topo')
@section('conteudo')
<div class="span12" style="margin-left: 0">
    <form method="get" action="{{url('produtos')}}">
        <div class="span3">
            <a href="{{url('produtos/create')}}" class="btn btn-success span12"><i class="icon-plus icon-white"></i> Adicionar Produto</a>
        </div>  
        <div class="span5">
            <input type="text" name="search"  id="pesquisa"  placeholder="Digite algo para pesquisar" class="span12 form-control" value="{{$request->get('search')}}" >    
        </div>        
        <div class="span1">
            <button class="span12 btn"> <i class="icon-search"></i> </button>
        </div>
        <div class="span1">
            <a href="{{url('relatorios/produtos?acao=imprimir')}}@if($request->get('search'))&search={{$request->get('search')}}@endif" class="span12 btn"> <i class="icon-print icon-white"></i></a>
        </div>
    </form>
</div>
<div class="span12" style="margin-left: 0">
    <div class="widget-box">
        @include('errors.mensagem')
        <div class="widget-title">
            <span class="icon">
                <i class="icon-barcode"></i>
            </span>
            <h5>Produtos</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Fornecedor</th>
                        <th>Marca</th>
                        <th>Estoque</th>
                        <th>Preço</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $produto)
                    <tr>
                        <td title="{{$produto->nome}}" >{{$produto->nome}}</td>
                        <td>{{$produto->categoria['nome']}}</td>
                        <td>{{$produto->fornecedor['nome']}}</td>
                        <td>{{$produto->marca['nome']}}</td>
                        <td class="@if($produto->estoque <= $produto->estoque_minimo) red @endif ">{{$produto->estoque}}</td>
                        <td>R$ {{$produto->preco_venda}}</td>
                        <td>
                            <a style="margin-right: 1%" href="{{url('produtos')}}/{{$produto->idProdutos}}" class="btn tip-top" title="Visualizar Produto"><i class="icon-eye-open"></i></a>
                            <a style="margin-right: 1%" href="{{url('produtos')}}/{{$produto->idProdutos}}/edit" class="btn btn-info tip-top" title="Editar Produto"><i class="icon-pencil icon-white"></i></a>
                            <a href="#modal-excluir" role="button" data-target="#modal-excluir" data-toggle="modal" produto="{{$produto->idProdutos}}" class="btn btn-danger tip-top" title="Excluir Produto"><i class="icon-remove icon-white"></i></a>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-offset-4">
                	
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="post" id="frm" name="frm">
                    <input name="_method" type="hidden" value="DELETE">
                    {!! csrf_field() !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Produto</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="idProduto" name="id" value="" />
                        <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button class="btn btn-danger">Excluir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .red{
        color: red;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var produto = $(this).attr('produto');
            $('#idProduto').val(produto);
            $("#frm").attr("action", "{{url('produtos')}}/" + produto);
        });
    });
</script>
@endsection
