@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Produto</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('produtos')}}" id="formProduto" method="post" class="form-horizontal" >
                    {!! csrf_field() !!}<br>
                     <div class="form-group">
                        <label for="nome" class="control-label col-md-2">Nome<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="descricao" class="form-control" type="text" name="nome" value="{{old('nome')}}"  />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="categoria_id" class="control-label col-md-2">Categoria<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="categoria_id" class="form-control">
                                <option>Selecione</option>
                                @foreach($categorias as $categoria)
                                <option value="{{$categoria->id}}">{{$categoria->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="fornecedor_id" class="control-label col-md-2">Fornecedor<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="fornecedor_id" class="form-control">
                                <option>Selecione</option>
                                @foreach($fornecedores as $fornecedor)
                                <option value="{{$fornecedor->id}}">{{$fornecedor->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="marca_id" class="control-label col-md-2">Marca<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="marca_id" class="form-control">
                                <option>Selecione</option>
                                @foreach($marcas as $fornecedor)
                                <option value="{{$fornecedor->id}}">{{$fornecedor->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="preco_compra" class="control-label col-md-2">Preço compra<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="preco_compra" class="form-control money" type="text" name="preco_compra" value="@if(old('custo') == null)0.00 @else {{old('custo')}} @endif"  />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="preco_venda" class="control-label col-md-2">Preço venda<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="preco_venda" class="form-control money" type="text" name="preco_venda" value="@if(old('venda') == null)0.00 @else {{old('venda')}} @endif"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="estoque_minimo" class="control-label col-md-2">Estoque Mínimo</label>
                        <div class="col-md-4">
                            <input id="estoque_minimo" class="form-control" type="text" name="estoque_minimo" value="{{old('estoque_minimo')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="{{url('produtos')}}" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

         </div>
     </div>
</div>

<script src="{{url('js/jquery.validate.js')}}"></script>
<script src="{{url('js/maskmoney.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#formProduto').validate({
            rules :{
                  nome: { required: true},
                  unidade: { required: true},
                  precoCompra: { required: true},
                  precoVenda: { required: true},
                  estoque: { required: true}
            },
            messages:{
                  nome: { required: 'Campo Requerido.'},
                  unidade: {required: 'Campo Requerido.'},
                  precoCompra: { required: 'Campo Requerido.'},
                  precoVenda: { required: 'Campo Requerido.'},
                  estoque: { required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
    });
</script>
@endsection
