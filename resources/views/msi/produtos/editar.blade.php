@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Produto</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('produtos')}}/{{$result->idProdutos}}" id="formProduto" method="post" class="form-horizontal" >
                    <input name="_method" type="hidden" value="PUT"><br>
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="nome" class="control-label col-md-2">Nome<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="descricao" class="form-control" type="text" name="nome" value="{{$result->nome}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="categoria_id" class="control-label col-md-2">Categoria<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="categoria_id" class="form-control">
                                <option>Selecione</option>
                                @foreach($dados['categorias'] as $categoria)
                                <option value="{{$categoria->id}}" @if($categoria->id == $result->categoria_id) selected @endif>{{$categoria->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="fornecedor_id" class="control-label col-md-2">Fornecedor<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="fornecedor_id" class="form-control">
                                <option>Selecione</option>
                                @foreach($dados['fornecedores'] as $fornecedor)
                                <option value="{{$fornecedor->id}}" @if($fornecedor->id == $result->fornecedor_id) selected @endif>{{$fornecedor->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="marca_id" class="control-label col-md-2">Marca<span class="required">*</span></label>
                        <div class="col-md-4">
                            <select name="marca_id" class="form-control">
                                <option>Selecione</option>
                                @foreach($dados['marcas'] as $marca)
                                <option value="{{$marca->id}}" @if($marca->id == $result->marca_id) selected @endif>{{$marca->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="preco_compra" class="control-label col-md-2">Preço compra<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="preco_compra" class="form-control money" type="text" name="preco_compra" value="{{$result->preco_compra}}"  />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="preco_venda" class="control-label col-md-2">Preço venda<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="preco_venda" class="form-control money" type="text" name="preco_venda" value="{{$result->preco_venda}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="estoque_minimo" class="control-label col-md-2">Estoque Mínimo</label>
                        <div class="col-md-4">
                            <input id="estoque_minimo" class="form-control" type="text" name="estoque_minimo" value="{{$result->estoque_minimo}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
                                <a href="{{url('produtos')}}" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<script src="{{url('js/jquery.validate.js')}}"></script>
<script src="{{url('js/maskmoney.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(".money").maskMoney();

    $('#formProduto').validate({
        rules: {
            descricao: {required: true},
            unidade: {required: true},
            precoCompra: {required: true},
            precoVenda: {required: true},
            estoque: {required: true}
        },
        messages: {
            descricao: {required: 'Campo Requerido.'},
            unidade: {required: 'Campo Requerido.'},
            precoCompra: {required: 'Campo Requerido.'},
            precoVenda: {required: 'Campo Requerido.'},
            estoque: {required: 'Campo Requerido.'}
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('error');
            $(element).parents('.form-group').addClass('success');
        }
    });
});
</script>
@endsection
