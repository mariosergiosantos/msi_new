@extends('tema.topo')
@section('conteudo')
<div class="accordion" id="collapse-group">
    <div class="accordion-group widget-box">
        <div class="accordion-heading">
            <div class="widget-title">
                <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                    <span class="icon"><i class="icon-list"></i></span><h5>Dados do Produto</h5>
                </a>
            </div>
        </div>
        <div class="collapse in accordion-body">
            <div class="widget-content">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td style="text-align: right; width: 30%"><strong>Nome</strong></td>
                            <td>{{$result->nome}}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Preço de Compra</strong></td>
                            <td>R$ {{$result->preco_compra}}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Preço de venda</strong></td>
                            <td>R$ {{$result->preco_venda}}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Estoque</strong></td>
                            <td>{{$result->estoque}}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Estoque Mínimo</strong></td>
                            <td>{{$result->estoque_minimo}}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>QRCode</strong></td>
                            <td><img src="{{qrCode('http://msifinanceiro.herokuapp.com/produtos/$result->idProdutos', 200, 200)}}" title="{{$result->descricao}}" /></td>
                        </tr>
                        <tr>
                            <td style="text-align: right"><strong>Código de Barras</strong></td>
                            <td>{!! $barcode->getBarcode($result->idProdutos, $barcode::TYPE_EAN_13) !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
