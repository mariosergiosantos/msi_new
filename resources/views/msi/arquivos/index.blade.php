@extends('tema.topo')
@section('conteudo')
<link rel="stylesheet" href="{{url('js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css')}}" />
<script type="text/javascript" src="{{url('js/jquery-ui/js/jquery-ui-1.9.2.custom.js')}}"></script>
<div class="span12" style="margin-left: 0">
    <form method="get" action="{{url('arquivos')}}">
        <div class="span3">
            <a href="{{url('arquivos/create')}}" class="btn btn-success span12"><i class="icon-plus icon-white"></i> Adicionar Arquivo</a>
        </div>  
        <div class="span5">
            <input type="text" name="search"  id="pesquisa"  placeholder="Digite o nome do documento para pesquisar" class="span12 form-control" value="{{$request->get('search')}}" >    
        </div>        
        <div class="span1">
            <button class="span12 btn"> <i class="icon-search"></i> </button>
        </div>
    </form>
</div>
<div class="span12" style="margin-left: 0">
    <div class="widget-box">
        @include('errors.mensagem')
        <div class="widget-title">
            <span class="icon">
                <i class="icon-hdd"></i>
            </span>
            <h5>Arquivos</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Documento</th>
                        <th>Tamanho</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $anexo)
                    <tr>
                        <th>{{$anexo->file}}</th>
                        <th>{{$anexo->tamanho}} Mb</th>
                        <th>
                            <a class="btn btn-inverse tip-top" style="margin-right: 1%" target="_blank" href="{{$anexo->url}}" class="btn tip-top" title="Imprimir"><i class="icon-print"></i></a>
                            <a href="{{url('arquivos/download')}}/{{$anexo->idDocumentos}}" class="btn tip-top" style="margin-right: 1%" title="Download"><i class="icon-download-alt"></i></a>
                            <a href="{{url('arquivos')}}/{{$anexo->idDocumentos}}/edit" class="btn btn-info tip-top" style="margin-right: 1%" title="Editar"><i class="icon-pencil icon-white"></i></a>
                            <a href="#modal-excluir" style="margin-right: 1%" role="button" data-toggle="modal" arquivo="{{$anexo->idDocumentos}}" class="btn btn-danger tip-top" title="Excluir Arquivo"><i class="icon-remove icon-white"></i></a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-offset-5">
                	
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="frm">
                <input name="_method" type="hidden" value="DELETE">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 id="myModalLabel">Excluir Arquivo</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idDocumento" name="id" value="" />
                    <h5 style="text-align: center">Deseja realmente excluir este arquivo?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    $(document).on('click', 'a', function (event) {
        var arquivo = $(this).attr('arquivo');
        $('#idDocumento').val(arquivo);
        $("#frm").attr("action", "{{url('arquivos')}}/" + arquivo);
    });
    $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
});
</script>
@endsection
