@extends('tema.topo')
@section('conteudo')
<link rel="stylesheet" href="{{url('js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css')}}" />
<script type="text/javascript" src="{{url('js/jquery-ui/js/jquery-ui-1.9.2.custom.js')}}"></script>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-hdd"></i>
                </span>
                <h5>Cadastro de Arquivo</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('arquivos')}}/{{$result->idDocumentos}}" id="formArquivo" method="post" class="form-horizontal" >
                    <input name="_method" type="hidden" value="PUT">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <label for="nome" class="control-label col-md-2">Nome do Arquivo*</label>
                        <div class="col-md-4">
                            <input id="nome" type="text" class="form-control" name="nome" value="{{$result->file}} " />
                            <input id="idDocumentos" type="hidden" name="idDocumentos" value="{{$result->idDocumentos}} " />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="descricao" class="control-label col-md-2">Descrição</label>
                        <div class="col-md-4">
                            <textarea rows="3" cols="30" class="form-control" name="descricao" id="descricao">{{$result->descricao}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
                                <a href="{{url('arquivos')}}" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{url('js/jquery.validate.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#formArquivo').validate({
        rules: {
            nome: {required: true}
        },
        messages: {
            nome: {required: 'Campo Requerido.'}
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').addClass('error');
            $(element).parents('.form-group').removeClass('success');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('error');
            $(element).parents('.form-group').addClass('success');
        }
    });
    $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
});
</script>
@endsection