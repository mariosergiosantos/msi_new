@extends('tema.topo')
@section('conteudo')
<link rel="stylesheet" href="{{url('js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css')}}" />
<script type="text/javascript" src="{{url('js/jquery-ui/js/jquery-ui-1.9.2.custom.js')}}"></script>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-hdd"></i>
                </span>
                <h5>Cadastro de Arquivo</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('arquivos')}}" id="formArquivo" enctype="multipart/form-data" method="post" class="form-horizontal" >
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="preco" class="control-label col-md-2"><span class="required">Arquivo*</span></label>
                        <div class="col-md-4">
                            <input id="arquivo" type="file" name="userfile" class="form-control"/> (txt|pdf|gif|png|jpg|jpeg)
                        </div>
                    </div>
                    
                    <!--<div class="form-group">
                        <label class="col-md-2 control-label ">Arquivo</label>
                        <div class="col-md-8">
                            <input type="file" name="arquivo" class="filestyle" data-buttonText="Escolha o arquivo" required>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <label for="nome" class="control-label col-md-2">Nome do Arquivo*</label>
                        <div class="col-md-4">
                            <input id="nome" type="text" name="nome" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="descricao" class="control-label col-md-2">Descrição</label>
                        <div class="col-md-4">
                            <textarea rows="3" cols="30" name="descricao" id="descricao" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="{{url('arquivos')}}" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{url('js/jquery.validate.js')}}"></script>
<script type="text/javascript">
      $(document).ready(function(){
           $('#formArquivo').validate({
            rules :{
                  nome:{ required: true},
                  userfile:{ required: true}
            },
            messages:{
                  nome :{ required: 'Campo Requerido.'},
                  userfile :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           }); 
           $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
      });
</script>
@endsection