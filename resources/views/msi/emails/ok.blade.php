<!DOCTYPE html>
<html lang="en">
  <head>
   <title>MSIFinanceiro</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
   
   <style>
	   .small {
		 font-size: 75% !important;
	   }
	   a{
		color:#00b7b5 !important;
		text-decoration: none;
	   }
	   .links{
		font-size:18px;
	   }
	   .container{
		padding: 20px;
		font-family: "Arial", sans-serif;
		color: #666;
	   }
	   #footer{
		font-size: 11px;
		line-height: 14px;
		font-weight: normal;
		padding: 0;
		text-align: left;
   }
   </style>
   </head>
   <body>
      <div class="container col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-4">
         <div id="header" class='row'>
            <img src="http://msifinanceiro.herokuapp.com/assets/img/logo.png" alt="Msifinanceiro"  class="">
         </div>
         <div id="content" class='row'>
		 <hr class='col-xs-10 col-sm-10 col-md-10'>
            <div class='col-xs-3 col-sm-2 col-md-2'>
               <img src="https://ingresso-a.akamaihd.net/checkout/img/email/icone-email.png" alt="E-mail" width="100" height="120" border="0" class="ecxsymbol ecxcenter-block ecxslideInDown ecxanimated">
            </div>
            <div class='col-xs-8 col-sm-5 col-md-6'>
               <strong>
                  <p>Pronto!</p>
               </strong>
               <span>Agora você pode desfrutar de sua conta.</span>
            </div>
         </div>
         <div id="footer" class='row small col-xs-12 col-sm-11 col-md-7'>
		 <hr>
            Para garantir o recebimento dos e-mails, adicione o remetente da <a href="http://msifinanceiro.herokuapp.com/" class='links-footer' target="_blank">msifinanceiro.com</a> aos seus contatos.<br>Esta mensagem é gerada automaticamente, portanto, não pode ser respondida. Caso você tenha dúvidas, por favor acesse o nosso <a href="http://msifinanceiro.herokuapp.com/rio-de-janeiro/home/institucional/atendimento/" class='links-footer' target="_blank">Atendimento.</a><br><br><a href="http://msifinanceiro.herokuapp.com/" target="_blank"><span class='links-footer'>www.msifinanceiro.com</span></a>
         </div>
      </div>
   </body>
</html>
