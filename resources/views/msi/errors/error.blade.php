<html>
<head>
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}"/>
    <title>Página não encontrada | 404 </title>

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Houve um erro ao processar o seu pedido.<br> O suporte já sabe do que se trata.</div>
        <a href="{{url('dashboard')}}" class="btn btn-primary col-md-4 col-md-offset-4">Voltar</a>
    </div>
</div>
</body>
</html>
