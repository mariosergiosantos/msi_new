@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Oops!</strong><br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (request()->session()->has('success'))
    <div class="alert alert-dismissable alert-success">
        {{request()->session()->get('success')}}
    </div>
@endif

@if (request()->session()->has('info'))
    <div class="alert alert-dismissable alert-info">
        {{request()->session()->get('info')}}
    </div>
@endif

@if (request()->session()->has('warning'))
    <div class="alert alert-dismissable alert-warning">
        {{request()->session()->get('warning')}}
    </div>
@endif

@if (request()->session()->has('danger'))
    <div class="alert alert-danger">
        {{request()->session()->get('danger')}}
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
@endif
