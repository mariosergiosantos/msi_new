@extends('tema.topo')
@section('conteudo')
<div class="span12" style="margin-left: 0; margin-top: 0">
    <!--Produtoss-->
    <div class="span12" style="margin-left: 0; margin-top: 0">
        <div class="widget-box" style="min-height: 200px">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-barcode"></i>
                </span>
                <h5>Produtos</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Categoria</th>
                            <th>Fornecedor</th>
                            <th>Estoque</th>
                            <th>Preço</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @if (count($data['produtos']) == 0)
                        <tr><td colspan="4">Nenhum produto foi encontrado.</td></tr>
                        @endif
                        @foreach ($data['produtos'] as $produto)
                        <tr>
                            <td>{{$produto->nome}}</td>
                            <td>{{$produto->categoria['nome']}}</td>
                            <td>{{$produto->fornecedor['nome']}}</td>
                            <td class="@if($produto->estoque <= $produto->estoqueMinimo) red @endif ">{{$produto->estoque}}</td>
                            <td>R$ {{$produto->precoVenda}}</td>
                            <td>
                                <a style="margin-right: 1%" href="{{url('produtos')}}/{{$produto->idProdutos}}" class="btn tip-top" title="Visualizar Produto"><i class="icon-eye-open"></i></a>
                                <a style="margin-right: 1%" href="{{url('produtos')}}/{{$produto->idProdutos}}/edit" class="btn btn-info tip-top" title="Editar Produto"><i class="icon-pencil icon-white"></i></a>
                        </tr>
                        @endforeach
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Clientes-->
    <div class="span12" style="margin-left: 0; margin-top: 0">
        <div class="widget-box" style="min-height: 200px">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Clientes</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF/CNPJ</th>
                            <th>Telefone</th>
                            <th>E-mail</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($data['clientes']) == 0)
                        <tr><td colspan="4">Nenhum cliente foi encontrado.</td></tr>
                        @endif

                        @foreach ($data['clientes'] as $cliente)
                        <tr>
                            <th>{{$cliente->nome}}</th>
                            <th>{{$cliente->documento}}</th>
                            <th>{{$cliente->telefone}}</th>
                            <th>{{$cliente->email}}</th>
                            <th>
                                <a style="margin-right: 1%" href="{{url('clientes')}}/{{$cliente->idClientes}}" class="btn tip-top" title="Visualizar Cliente"><i class="icon-eye-open"></i></a> 
                                <a style="margin-right: 1%" href="{{url('clientes')}}/{{$cliente->idClientes}}/edit" class="btn btn-info tip-top" title="Editar Cliente"><i class="icon-pencil icon-white"></i></a>
                            </th>
                        </tr>
                        @endforeach
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Fornecedores-->
    <div class="span12" style="margin-left: 0; margin-top: 0">
        <div class="widget-box" style="min-height: 200px">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Fornecedores</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CNPJ</th>
                            <th>Telefone</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @if (count($data['fornecedores']) == 0) 
                        <tr><td colspan="4">Nenhum fornecedor foi encontrado.</td></tr>
                        @endif
                        @foreach($data['fornecedores'] as $fornecedor)
                        <tr>
                            <th>{{$fornecedor->nome}}</th>
                            <th>{{$fornecedor->cnpj}}</th>
                            <th>{{$fornecedor->telefone}}</th>
                            <th>
                                <a style="margin-right: 1%" href="{{url('fornecedores')}}/{{$fornecedor->id}}" class="btn tip-top" title="Visualizar Fornecedor"><i class="icon-eye-open"></i></a> 
                                <a style="margin-right: 1%" href="{{url('fornecedores')}}/{{$fornecedor->id}}/edit" class="btn btn-info tip-top" title="Editar Fornecedor"><i class="icon-pencil icon-white"></i></a>
                            </th>
                        </tr>
                        @endforeach
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--arquivos-->
    <div class="span12" style="margin-left: 0; margin-top: 0">
        <div class="widget-box" style="min-height: 200px">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Arquivos</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th>Documento</th>
                            <th>Tamanho</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @if (count($data['arquivos']) == 0)
                        <tr><td colspan="4">Nenhum arquivo foi encontrado.</td></tr>
                        @endif

                        @foreach($data['arquivos'] as $anexo)
                        <tr>
                            <th>{{$anexo->file}}</th>
                            <th>{{$anexo->tamanho}} Mb</th>
                            <th>
                                <a class = "btn btn-inverse tip-top" style = "margin-right: 1%" target = "_blank" href = "{{$anexo->url}}" class = "btn tip-top" title = "Imprimir"><i class = "icon-print"></i></a>
                                <a href = "{{url('arquivos/download')}}/{{$anexo->idDocumentos}}" class = "btn tip-top" style = "margin-right: 1%" title = "Download"><i class = "icon-download-alt"></i></a>
                                <a href = "{{url('arquivos')}}/{{$anexo->idDocumentos}}/edit" class = "btn btn-info tip-top" style = "margin-right: 1%" title = "Editar"><i class = "icon-pencil icon-white"></i></a>
                            </th>
                        </tr>
                        @endforeach
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<style>
    .red{
        color: red;
    }
</style>
@endsection

