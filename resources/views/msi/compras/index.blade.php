@extends('tema.topo')
@section('conteudo')
<a href="{{url('vendas/create')}}" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Venda</a>
<div class="widget-box">
    @include('errors.mensagem')
    <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
        </span>
        <h5>Vendas</h5>
    </div>
    <div class="widget-content nopadding">
        <table class="table table-bordered ">
            <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Código</th>
                    <th><strong>Data da Venda</strong></th>
                    <th><strong>Cliente</strong></th>
                    <th>Pago</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $venda)
                <tr>
                    <th>#{{$venda->idVendas}}</th>
                    <th>{{date('d/m/Y', strtotime($venda->created_at))}}</th>
                    <th>{{$venda->cliente['nome']}}</th>
                    <th>
                        @if($venda->faturado == 1)
                        Sim
                        @else
                        Não
                        @endif

                    </th>
                    <th>
                        <a style="margin-right: 1%" href="{{url('vendas')}}/{{$venda->idVendas}}" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>
                        <a style="margin-right: 1%" href="{{url('vendas')}}/{{$venda->idVendas}}/edit" class="btn btn-info tip-top" title="Editar venda" @if($venda->faturado == 1) disabled='true' @endif><i class="icon-pencil icon-white"></i></a>
                        <a href="#modal-excluir" role="button" data-toggle="modal" venda="{{$venda->idVendas}}" class="btn btn-danger tip-top" title="Excluir Venda" @if($venda->faturado == 1) disabled='true' @endif><i class="icon-remove icon-white"></i></a>
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="col-lg-offset-5">
            {!! $results->render() !!}	
        </div>
    </div>
</div>

<div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="frm">
                <input name="_method" type="hidden" value="DELETE">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 id="myModalLabel">Excluir Venda</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idVenda" name="id" value="" />
                    <h5 style="text-align: center">Deseja realmente excluir esta Venda?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var venda = $(this).attr('venda');
            $('#idVenda').val(venda);
            $("#frm").attr("action", "{{url('vendas')}}/" + venda);
        });
    });
</script>
@endsection
