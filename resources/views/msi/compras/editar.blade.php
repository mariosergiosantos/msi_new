@extends('tema.topo') 
@section('conteudo')
<link rel="stylesheet" href="{{url('js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css')}}" />
<script type="text/javascript" src="{{url('js/jquery-ui/js/jquery-ui-1.9.2.custom.js')}}"></script>

<div class="row-fluid" style="margin-top: 0">
    <div class="col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon"> <i class="icon-tags"></i>
                </span>
                <h5>Editar Venda</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="col-md-12" id="divProdutosServicos" style="margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da Venda</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="col-md-12" id="divEditarVenda">
                                <form action="{{url('vendas')}}" method="post" id="formVendas"  class="form-horizontal">
                                    <div class="col-md-12"
                                         style="padding: 1%; margin-left: 0">
                                        <h3>#Venda: {{ $result->idVendas }}</h3>
                                        <div class="col-md-2" style="margin-left: 0">
                                            <label for="dataFinal">Data</label> 
                                            <input
                                                id="dataVenda" class="col-md-12 datepicker form-control" disabled type="text"
                                                name="dataVenda"
                                                value="{{date('Y-m-d', strtotime($result->created_at))}}" />
                                        </div>
                                        <div class="col-md-5">
                                            <label for="cliente">Cliente<span class="required">*</span></label>
                                            <input id="cliente" class="col-md-12 form-control" disabled type="text" name="cliente" value="{{ $result->cliente['nome'] }}" />
                                            <input id="clientes_id" class="col-md-12" type="hidden" name="clientes_id" value="{{ $result->cliente['clientes_id'] }}" /> 
                                        </div>
                                        <div class="col-md-5">
                                            <label for="tecnico">Vendedor<span class="required">*</span></label>
                                            <input id="tecnico" class="col-md-12 form-control" disabled type="text" name="tecnico"  value="{{ $result->usuario['nome'] }}" /> 
                                            <input id="usuarios_id" class="col-md-12" type="hidden" name="usuarios_id" value="{{ $result->usuario['usuarios_id'] }}" />
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-8 offset2" style="text-align: center">
                                            @if ($result->faturado == 0) 
                                            <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Faturar</a>
                                            @endif
                                            <a href="{{url('vendas')}}/{{ $result->idVendas }}" class="btn btn-inverse"><i class="icon-eye-open"></i>Visualizar Venda</a> <a href="{{url('vendas')}}" class="btn">
                                                <i class="icon-arrow-left"></i> Voltar
                                            </a>
                                        </div>
                                    </div>
                                </form>

                                <div class="col-md-12 well" style="padding: 1%; margin-left: 0">
                                    <form id="formProdutos"
                                          action="{{url('vendas/adicionarProduto')}}"
                                          method="post">
                                        <div class="col-md-8">
                                            <input type="hidden" 
                                                   name="produtos_id"
                                                   id="produtos_id" />
                                            <input type="hidden"
                                                   name="vendas_id"
                                                   id="vendas_id"
                                                   value="{{ $result->idVendas }}" /> 
                                            <input type="hidden"
                                                   name="preco"
                                                   id="preco"
                                                   value="" />

                                            <input id="subTotal" type="hidden" name="subTotal" value="" />
                                            <label for="">Produto</label> 
                                            <input type="text" class="col-md-12 form-control"
                                                   name="produto" id="produto"
                                                   placeholder="Digite o nome ou código do produto" />
                                        </div>
                                        <div class="col-md-2">
                                            <label for="">Quantidade</label>
                                            <input type="text" 
                                                   placeholder="Quantidade"
                                                   id="quantidade"
                                                   name="quantidade"
                                                   class="col-md-12 form-control" />
                                        </div>
                                        <div class="col-md-2">
                                            <label for="">&nbsp</label>
                                            <button class="btn btn-success col-md-12"
                                                    id="btnAdicionarProduto">
                                                <i class="icon-white icon-plus"></i> Adicionar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12" id="divProdutos" style="margin-left: 0">
                                    <table class="table table-bordered" id="tblProdutos">
                                        <thead>
                                            <tr>
                                                <th>Produto</th>
                                                <th>Quantidade</th>
                                                <th>Preço Unitário</th>
                                                <th>Ações</th>
                                                <th>Sub-total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total = 0;
                                            ?>

                                            @foreach($produtos as $produto)
                                            <tr>
                                                <th>{{$produto->produtos['idProdutos']}} <br>  {{$produto->produtos['nome']}}</th>
                                                <th>{{$produto->quantidade}}</th>
                                                <th>{{$produto->produtos['preco_venda']}}</th>
                                                <th><a idAcao="{{$produto->idItens}}" prodAcao="{{$produto->idProdutos}}" quantAcao="{{$produto->quantidade}}" title="Excluir Produto" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></th>
                                                <th>R$ {{number_format($produto->subTotal, 2, ',', '.')}}</th>
                                            </tr>
                                            <?php
                                            $total += $produto->subTotal;
                                            ?>
                                            @endforeach

                                            <tr>
                                                <td colspan="4" style="text-align: right"><strong>Total:</strong></td>
                                                <td><strong>R$ {{number_format($total, 2, ',', '.') }}</strong> <input type="hidden" id="total-venda" value="{{ number_format($total, 2) }}"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <input type="hidden" id="valueParcelamento" value="{{$result->parcelamento}}">

                                <div class="col-md-12 well" id="divPagamento" style="padding: 1%; margin-left: 0">
                                    <form action="{{url('vendas/forma-pagamento')}}" method="POST" id="frmFormaPagamento" name="frmFormaPagamento">
                                        <div class="col-md-6">
                                            <label for="forma_pagamento_id">Forma de pagamento</label>
                                            <select name="forma_pagamento_id" id="forma_pagamento_id" class="col-md-12 form-control">
                                                <option>Selecione</option>
                                                @foreach($formaPagamentos as $fp)
                                                <option value="{{$fp->id}}" @if($fp->id == $result->forma_pagamento_id) selected @endif >{{$fp->nome}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <input type="hidden" name="idVendas" value="{{$result->idVendas}}">
                                        {!! csrf_field() !!}

                                        <div class="col-md-6">
                                            <label for="parcelamento">Parcelamento</label>
                                            <select name="parcelamento" id="parcelamento" class="col-md-12 form-control">
                                                <option>Selecione</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <label for="data_vencimento">Data de vencimento</label>
                                            <input id="data_vencimento" class="datepicker form-control" type="text"
                                                   name="data_vencimento" value=" @if($result->data_vencimento == null) {{dateCurrent()}} @else {{$result->data_vencimento}}  @endif" />
                                        </div>

                                        <div class="col-md-2 col-md-offset-7">
                                            <label for="">&nbsp</label>
                                            <button class="btn btn-success col-md-12"
                                                    id="btnAdicionarFormaPagamento">
                                                <i class="icon-white"></i> Salvar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                .
            </div>
        </div>
    </div>
</div>

<!-- Modal Faturar-->
<div class="modal fade" id="modal-faturar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="formFaturar" action="{{url('vendas')}}/{{$result->idVendas}}" method="post">
                <input name="_method" type="hidden" value="PUT">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Faturar venda</h4>
                </div>
                <div class="modal-body hidden" id="not_pagament_defined">
                   <div class="col-md-12 alert alert-danger">
                        Antes de faturar a venda é necessario selecionar uma forma de pagamento
                    </div>
                </div>
                <div class="modal-body" id="pagament_defined">
                    <div class="col-md-12 alert alert-info" style="margin-left: 0">
                        Obrigatório o preenchimento dos campos com asterisco.
                    </div>
                    <div class="col-md-12 form-group" style="margin-left: 0">
                        <label for="descricao">Descrição</label>
                        <input class="col-md-12 form-control" id="descricao" 
                               type="text" name="descricao"
                               value="Fatura de Venda - #{{ $result->idVendas }}" />
                    </div>
                    <div class="col-md-12 form-group" style="margin-left: 0">
                        <label for="cliente">Cliente*</label> 
                        <input class="col-md-12 form-control" 
                               id="cliente"
                               type="text"
                               value="{{ $result->cliente['nome'] }}" disabled="" />

                        <input type="hidden" name="faturado" value="1"/>
                        <input type="hidden" name="valor_total"
                               value="{{ number_format($total, 2) }} " />
                    </div>

                    <div class="col-md-12 form-group" style="margin-left: 0">
                        <label for="valor_total">Valor*</label>
                        <input class="col-md-12 money form-control"
                               id="valor_total" type="text" name="valor_total" disabled
                               value="{{ number_format($total, 2) }} " />
                    </div>
                    <div class="col-md-12 form-group" style="margin-left: 0">
                        <label for="faturado">Recebido?</label>
                        <input id="faturado" type="checkbox" value="1" checked="" disabled=""/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
                    <button class="btn btn-primary">Faturar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{url('js/jquery.validate.js')}}"></script>
<script src="{{url('js/maskmoney.js')}}"></script>
<script src="{{url('js/jquery.maskedinput.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {

    //Atualiza o select de parcelamento
    updateFormaPagamento();

    var progress = "<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>";

    $(".money").maskMoney();

    $('#recebido').click(function (event) {
        var flag = $(this).is(':checked');
        if (flag == true) {
            $('#divRecebimento').show();
        }
        else {
            $('#divRecebimento').hide();
        }
    });
<?php

include_once "org_netbeans_saas_google/GoogleAccountsService.php";
             try {
                 $accountType = null;
                 $email = "";
                 $passwd = "";
                 $service = "";
                 $source = null;

             $result = GoogleAccountsService::accountsClientLogin($accountType, $email, $passwd, $service, $source);
             echo $result->getResponseBody();
             } catch(Exception $e) {
                 echo "Exception occured: ".$e;
             }

?>



    $(document).on('click', '#btn-faturar', function (event) {
        event.preventDefault();
        valor = $('#total-venda').val();
        valor = valor.replace(',', '');
        var forma_pagamento = "{{$result->forma_pagamento_id}}";
        var parcelamento = "{{$result->parcelamento}}";
        if(forma_pagamento == null || forma_pagamento == "" ||
               parcelamento == null || parcelamento == "" ){
            $("#not_pagament_defined").removeClass("hidden");
            $("#pagament_defined").addClass("hidden");
        }
        console.log(forma_pagamento);
        $('#valor').val(valor);
    });

    $("#formFaturar").validate({
        rules: {
            descricao: {required: true},
            cliente: {required: true},
            valor: {required: true},
            vencimento: {required: true}
        },
        messages: {
            descricao: {required: 'Campo Requerido.'},
            cliente: {required: 'Campo Requerido.'},
            valor: {required: 'Campo Requerido.'},
            vencimento: {required: 'Campo Requerido.'}
        }
    });

    $("#cliente").autocomplete({
        source: "{{url('autocomplete/cliente')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#clientes_id").val(ui.item.id);
        }
    });

    $("#tecnico").autocomplete({
        source: "{{url('autocomplete/usuario')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#usuarios_id").val(ui.item.id);
        }
    });

    $("#formVendas").validate({
        rules: {
            cliente: {required: true},
            tecnico: {required: true},
            dataVenda: {required: true}
        },
        messages: {
            cliente: {required: 'Campo Requerido.'},
            tecnico: {required: 'Campo Requerido.'},
            dataVenda: {required: 'Campo Requerido.'}
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    $("#produto").autocomplete({
        source: "{{url('autocomplete/produto')}}",
        minLength: 2,
        select: function (event, ui) {
            $("#produtos_id").val(ui.item.id);
            $("#estoque").val(ui.item.estoque);
            $("#preco").val(ui.item.preco_venda);
            $("#quantidade").focus();
        }
    });

    $("#formProdutos").validate({
        rules: {
            produto: {required: true},
            quantidade: {required: true}
        },
        messages: {
            produto: {required: "Selecione um produto"},
            quantidade: {required: 'Insira a quantidade'}
        },
        submitHandler: function (form) {
            var quantidade = parseInt($("#quantidade").val());
            $("#subTotal").val(parseFloat($("#preco").val()) * quantidade);
            var dados = $(form).serialize();
            $("#divProdutos").html(progress);
            $.ajax({
                type: "POST",
                url: "{{url('vendas/produto')}}",
                data: dados,
                dataType: 'json',
                success: function (data) {
                    if (!data.result) {
                        alert(data.message);
                        return false;
                    }
                    $("#quantidade").val('');
                    $("#produto").val('').focus();
                    $("#divProdutos").load("{{request()->getUri()}} #divProdutos");
                }
            });
            return false;
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    $("#frmFormaPagamento").validate({
        rules: {
            forma_pagamento_id: {required: true},
            parcelamento: {required: true}
        },
        messages: {
            forma_pagamento_id: {required: "Selecione um produto"},
            parcelamento: {required: 'Insira a quantidade'}
        },
        submitHandler: function (form) {
            var dados = $(form).serialize();
            $("#progress").html(progress);
            $("#btnAdicionarFormaPagamento").attr('disabled');
            $.ajax({
                type: "POST",
                data: dados,
                url: "{{url('vendas/forma-pagamento')}}",
                dataType: 'json',
                success: function (data) {
                    //$("#progress").html("");
                    if (!data.result) {
                        alert(data.message);
                    } else {
                        alert("Forma de pagamento efetuada com sucesso!");
                    }
                }
            });
            return false;
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });


    $(document).on('click', 'a', function (event) {
        var idProduto = $(this).attr('idAcao');
        if ((idProduto % 1) == 0) {
            $("#divProdutos").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>");
            $.ajax({
                type: "DELETE",
                url: "{{url('vendas/produto')}}/" + idProduto,
                dataType: 'json',
                success: function (data) {
                    if (!data.result) {
                        alert(data.message);
                    }
                    $("#divProdutos").load("{{request()->getUri()}} #divProdutos");
                }
            });
            return false;
        }
    });

    $("#forma_pagamento_id").change(function () {
        updateFormaPagamento();
    });

    function updateFormaPagamento() {
        var formaDePagamento = $("#forma_pagamento_id").val();
        var parcelamento = $("#parcelamento");
        $.ajax({
            url: "{{url('vendas/opcoesParcelamento')}}/" + formaDePagamento,
            dataType: 'json',
            success: function (data) {
                parcelamento.empty();
                parcelamento.append("<option>Selecione</option>");
                if (data.length < 1) {
                    parcelamento.append("<option value='1'>A Vista</option>");
                }

                for (var i = 0; i < data.length; i++) {
                    var selec = "";
                    if ($("#valueParcelamento") != null) {
                        if (data[i]['id'] == $("#valueParcelamento").val()) {
                            selec = "selected";
                        }
                    }

                    parcelamento.append("<option " + selec + " value='" + data[i]['id'] + "'>" + data[i]['nome'] + "</option>");
                }
            }
        });
    }

    $(".datepicker").datepicker();
    //$(".datepicker").mask("99-99-9999");
    $(".datepicker").mask("9999-99-99");
});
</script>
@endsection
