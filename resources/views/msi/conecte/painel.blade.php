@extends('tema.topo')
@section('conteudo')
<div class="quick-actions_homepage">
    <ul class="quick-actions">
        <li class="bg_lo span3"> <a href="{{url('index.php/conecte/os')}}"> <i class="icon-tags"></i> Ordens de Serviço</a> </li>
        <li class="bg_ls span3"> <a href="{{url('index.php/conecte/compras')}}"><i class="icon-shopping-cart"></i> Compras</a></li>
        <li class="bg_lg span3"> <a href="{{url('index.php/conecte/conta')}}"><i class="icon-star"></i> Minha Conta</a></li>
    </ul>
</div>

<div class="span12" style="margin-left: 0">

    <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Últimas Ordens de Serviço</h5></div>
        <div class="widget-content">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Data Inicial</th>
                        <th>Data Final</th>
                        <th>Garantia</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    echo '<tr><td colspan="3">Nenhum ordem de serviço encontrada.</td></tr>';
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Últimas Compras</h5></div>
        <div class="widget-content">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Data de Venda</th>
                        <th>Responsável</th>
                        <th>Faturado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    echo '<tr><td colspan="5">Nenhum venda encontrada.</td></tr>';
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection