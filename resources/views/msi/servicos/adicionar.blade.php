@extends('tema.topo')
@section('conteudo')
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Serviço</h5>
            </div>
            <div class="widget-content nopadding">
                <form action="{{url('servicos')}}" id="formServico" method="post" class="form-horizontal" >
                    {!! csrf_field() !!}<br>
                    <div class="form-group">
                        <label for="nome" class="control-label col-md-2">Nome<span class="required">*</span></label>
                        <div class="col-md-4">
                            <input id="nome" type="text" class="form-control" name="nome" value="{{old('nome')}}"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="preco" class="control-label col-md-2"><span class="required">Preço*</span></label>
                        <div class="col-md-4">
                            <input id="preco" class="form-control money" type="text" name="preco" value="{{old('preco')}}"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descricao" class="control-label col-md-2">Descrição</label>
                        <div class="col-md-4">
                            <input id="descricao" class="form-control" type="text" name="descricao" value="{{old('descricao')}}"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="span12">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="{{url('servicos')}}" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{url('js/jquery.validate.js')}}"></script>
<script src="{{url('js/maskmoney.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(".money").maskMoney();
    $('#formServico').validate({
        rules: {
            nome: {required: true},
            preco: {required: true}
        },
        messages: {
            nome: {required: 'Campo Requerido.'},
            preco: {required: 'Campo Requerido.'}
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
});
</script>
@endsection
