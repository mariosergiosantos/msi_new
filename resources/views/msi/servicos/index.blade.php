@extends('tema.topo')
@section('conteudo')
<a href="{{url('servicos/create')}}" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Serviço</a>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="icon-wrench"></i>
        </span>
        <h5>Serviços</h5>
    </div>
    <div class="widget-content nopadding">
        <table class="table table-bordered ">
            <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Nome</th>
                    <th>Preço</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $servico)
                <tr>
                    <td>{{$servico->nome}}</td>
                    <td>R$ {{number_format($servico->preco,2,',','.')}}</td>
                    <td>{{$servico->descricao}}</td>
                    <td>
                        <a style="margin-right: 1%" href="{{url('servicos')}}/{{$servico->idServicos}}/edit" class="btn btn-info tip-top" title="Editar Serviço"><i class="icon-pencil icon-white"></i></a>
                        <a href="#modal-excluir" role="button" data-toggle="modal" data-target="#modal-excluir" servico="{{$servico->idServicos}}" class="btn btn-danger tip-top" title="Excluir Serviço"><i class="icon-remove icon-white"></i></a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="col-lg-offset-5">
            {!! $results->render() !!}	
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id="frm" name="frm">
                <input name="_method" type="hidden" value="DELETE">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Excluir Serviço</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idServico" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir este serviço?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var servico = $(this).attr('servico');
            $('#idServico').val(servico);
            $("#frm").attr("action", "{{url('servicos')}}/" + servico);
        });
    });
</script>
@endsection
