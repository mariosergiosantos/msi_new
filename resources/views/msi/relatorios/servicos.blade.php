@extends('tema.topo')
@section('conteudo')
    <div class="row-fluid" style="margin-top: 0">
        <div class="span4">
            <div class="widget-box">
                <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                    <h5>Relatórios Rápidos</h5>
                </div>
                <div class="widget-content">
                    <ul class="site-stats">
                        <li>
                            <a href="{{url('relatorios/servicos?acao=imprimir')}}"><i
                                        class="icon-wrench"></i>
                                <small>Todos os Serviços</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="span8">
            <div class="widget-box">
                <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                    <h5>Relatórios Customizáveis</h5>
                </div>
                <div class="widget-content">
                    <div class="span12 well">
                        <form action="{{url('relatorios/servicos')}}" method="get">
                            <div class="span12 well">
                                <div class="span6">
                                    <label for="">Preço de:</label>
                                    <input type="text" name="precoInicial" value="0.00" class="span12 money"/>
                                </div>
                                <div class="span6">
                                    <label for="">até:</label>
                                    <input type="text" name="precoFinal" value="0.00" class="span12 money"/>
                                </div>
                            </div>

                            <div class="span12" style="margin-left: 0; text-align: center">
                                <input type="reset" class="btn" value="Limpar"/>
                                <button class="btn btn-inverse"><i class="icon-print icon-white"></i> Imprimir</button>
                            </div>
                            <input type="hidden" value="imprimir" name="acao">
                        </form>
                    </div>
                    .
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('js/maskmoney.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".money").maskMoney();
        });
    </script>
@endsection