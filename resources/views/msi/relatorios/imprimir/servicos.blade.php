@extends('relatorios.imprimir.main')
@section('conteudo')
<body style="background-color: transparent">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Serviços</h4>
                </div>
                <div class="widget-content nopadding">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="font-size: 1.2em; padding: 5px;">Nome</th>
                            <th style="font-size: 1.2em; padding: 5px;">Descrição</th>
                            <th style="font-size: 1.2em; padding: 5px;">Preço</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($servicos as $s) {

                            echo '<tr>';
                            echo '<td>' . $s->nome . '</td>';
                            echo '<td>' . $s->descricao . '</td>';
                            echo '<td>' . $s->preco . '</td>';

                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>
        </div>
    </div>
</div>
@endsection