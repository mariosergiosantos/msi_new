<head>
    <title>MAPOS</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{url('css/bootstrap-responsive.min.css')}}"/>
    <link rel="stylesheet" href="{{url('css/fullcalendar.css')}}"/>
    <link rel="stylesheet" href="{{url('css/main.css')}}"/>
    <link rel="stylesheet" href="{{url('css/blue.css')}}" class="skin-color"/>
    <script type="text/javascript" src="{{url('js/jquery-1.10.2.min.js')}}"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>


@yield('conteudo')

<!-- Arquivos js-->
<script src="{{url('js/excanvas.min.js')}}"></script>
<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="{{url('js/jquery.flot.min.js')}}"></script>
<script src="{{url('js/jquery.flot.resize.min.js')}}"></script>
<script src="{{url('js/jquery.peity.min.js')}}"></script>
<script src="{{url('js/fullcalendar.min.js')}}"></script>
<script src="{{url('js/sosmc.js')}}"></script>
<script src="{{url('js/dashboard.js')}}"></script>
</body>
</html>