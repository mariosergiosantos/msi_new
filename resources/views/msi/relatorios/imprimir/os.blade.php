@extends('relatorios.imprimir.main')
@section('conteudo')
<body style="background-color: transparent">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Ordens de Serviço</h4>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="font-size: 1.2em; padding: 5px;">Cliente</th>
                            <th style="font-size: 1.2em; padding: 5px;">Status</th>
                            <th style="font-size: 1.2em; padding: 5px;">Data</th>
                            <th style="font-size: 1.2em; padding: 5px;">Descrição</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($os as $c) {
                            echo '<tr>';
                            echo '<td>' . $c->cliente['nomeCliente'] . '</td>';
                            echo '<td>' . $c->status . '</td>';
                            echo '<td>' . date('d/m/Y', strtotime($c->dataInicial)) . '</td>';
                            echo '<td>' . $c->descricaoProduto . '</td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>
        </div>
    </div>
</div>
@endsection