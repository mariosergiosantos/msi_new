@extends('relatorios.imprimir.main')
@section('conteudo')
<body style="background-color: transparent">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Clientes</h4>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="font-size: 1.2em; padding: 5px;">Nome</th>
                            <th style="font-size: 1.2em; padding: 5px;">Documento</th>
                            <th style="font-size: 1.2em; padding: 5px;">Telefone</th>
                            <th style="font-size: 1.2em; padding: 5px;">Email</th>
                            <th style="font-size: 1.2em; padding: 5px;">Cadastro</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($clientes as $c) {
                            $dataCadastro = date('d/m/Y', strtotime($c->dataCadastro));
                            echo '<tr>';
                            echo '<td>' . $c->nomeCliente . '</td>';
                            echo '<td>' . $c->documento . '</td>';
                            echo '<td>' . $c->telefone . '</td>';
                            echo '<td>' . $c->email . '</td>';
                            echo '<td>' . $dataCadastro . '</td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection