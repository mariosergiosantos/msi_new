@extends('angle::auth.angle-auth')
@section('content')
<!-- START panel-->
<div class="panel panel-dark panel-flat">
    <div class="panel-heading text-center">
        <a href="#">
            <img src="{{url('img/logo.png')}}" alt="Image" class="block-center img-rounded">
        </a>
    </div>
    <div class="panel-body">
        <p class="text-center pv">ENTRE PARA CONTINUAR.</p>
        @include('angle::error.messege')
        <form role="form" data-parsley-validate="" novalidate="" class="mb-lg" method="post" action="{{url('auth/login')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <input id="exampleInputEmail1" type="email" name="email" placeholder="Enter email" autocomplete="off" required class="form-control">
                <span class="fa fa-envelope form-control-feedback text-muted"></span>
            </div>
            <div class="form-group has-feedback">
                <input id="exampleInputPassword1" type="password" name="password" placeholder="Password" required class="form-control">
                <span class="fa fa-lock form-control-feedback text-muted"></span>
            </div>
            <div class="clearfix">
                <div class="checkbox c-checkbox pull-left mt0">
                    <label>
                        <input type="checkbox" name="remember">
                        <span class="fa fa-check"></span>Lembrar-me</label>
                </div>
                <div class="pull-right"><a href="{{url('password/email')}}" class="text-muted">Esqueceu sua senha?</a>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>
        </form>
        <p class="pt-lg text-center">Ainda não tem cadastro?</p><a href="{{url('auth/register')}}" class="btn btn-block btn-default">Cadastre agora</a>
    </div>
</div>
@endsection
