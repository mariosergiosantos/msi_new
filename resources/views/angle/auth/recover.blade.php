@extends('angle::auth.angle-auth')
@section('content')
<!-- START panel-->
<!-- START panel-->
<div class="panel panel-dark panel-flat">
    <div class="panel-heading text-center">
        <a href="#">
            <img src="{{url('img/logo.png')}}" alt="Image" class="block-center img-rounded">
        </a>
    </div>
    <div class="panel-body">
        <p class="text-center pv">RESETAR SENHA</p>
        @include('angle::error.messege')
        <form role="form" method="post" action="{{url('password/email')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <p class="text-center">Preencha com seu e-mail para receber instruções sobre como redefinir sua senha.</p>
            <div class="form-group has-feedback">
                <label for="resetInputEmail1" class="text-muted">E-mail</label>
                <input id="resetInputEmail1" type="email" name="email" placeholder="Enter email" autocomplete="off" class="form-control">
                <span class="fa fa-envelope form-control-feedback text-muted"></span>
            </div>
            <button type="submit" class="btn btn-danger btn-block">Resetar</button>
        </form>
    </div>
</div>
@endsection