@extends('angle::auth.angle-auth')
@section('content')
<!-- START panel-->
<div class="panel panel-dark panel-flat">
    <div class="panel-heading text-center">
        <a href="#">
            <img src="{{url('img/logo.png')}}" alt="Image" class="block-center img-rounded">
        </a>
    </div>
    <div class="panel-body">
        <p class="text-center pv">REGISTE PARA TER ACESSO IMEDIATO.</p>
        @include('angle::error.messege')
        <form role="form" data-parsley-validate="" novalidate="" class="mb-lg" method="post" action="{{url('auth/register')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <label for="signupInputEmail1" class="text-muted">Endereço de e-mail</label>
                <input id="signupInputEmail1" type="email" name="email" placeholder="Enter email" required class="form-control">
                <span class="fa fa-envelope form-control-feedback text-muted"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="signupInputPassword1" class="text-muted">Senha</label>
                <input id="signupInputPassword1" type="password" name="password" placeholder="Password" autocomplete="off" required class="form-control">
                <span class="fa fa-lock form-control-feedback text-muted"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="signupInputRePassword1" class="text-muted">Repita a senha</label>
                <input id="signupInputRePassword1" type="password" placeholder="Retype Password" autocomplete="off" required data-parsley-equalto="#signupInputPassword1" class="form-control">
                <span class="fa fa-lock form-control-feedback text-muted"></span>
            </div>
            <div class="clearfix">
                <div class="checkbox c-checkbox pull-left mt0">
                    <label>
                        <input type="checkbox" value="" required name="terms">
                        <span class="fa fa-check"></span>Concordo com os <a href="#">termos</a>
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-block btn-primary mt-lg">Criar conta</button>
        </form>
        <p class="pt-lg text-center">Já é cadastrado?</p><a href="{{url('auth/login')}}" class="btn btn-block btn-default">Login</a>
    </div>
</div>
@endsection