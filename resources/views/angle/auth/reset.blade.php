@extends('angle::auth.angle-auth')
@section('content')
<!-- START panel-->
<div class="panel panel-dark panel-flat">
    <div class="panel-heading text-center">
        <a href="#">
            <img src="{{url('img/logo.png')}}" alt="Image" class="block-center img-rounded">
        </a>
    </div>
    <div class="panel-body">
        <p class="text-center pv">RESETAR SENHA</p>
        @include('angle::error.messege')
        <form role="form" method="post" action="{{url('password/reset')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback">
                <label for="signupInputEmail1" class="text-muted">Endereço de e-mail</label>
                <input id="signupInputEmail1" type="email" name="email" placeholder="Enter email" required class="form-control">
                <span class="fa fa-envelope form-control-feedback text-muted"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="signupInputPassword1" class="text-muted">Senha</label>
                <input id="signupInputPassword1" type="password" name="password" placeholder="Password" autocomplete="off" required class="form-control">
                <span class="fa fa-lock form-control-feedback text-muted"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="signupInputRePassword1" class="text-muted">Repita a senha</label>
                <input id="signupInputRePassword1" type="password" placeholder="Retype Password" autocomplete="off" required data-parsley-equalto="#signupInputPassword1" class="form-control">
                <span class="fa fa-lock form-control-feedback text-muted"></span>
            </div>
            <button type="submit" class="btn btn-block btn-primary mt-lg">Resetar senha</button>
        </form>
    </div>
</div>
@endsection