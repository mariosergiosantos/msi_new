@extends('angle::auth.angle-auth')
@section('content')
<!-- START panel-->
<div class="text-center mb-xl">
    <div class="text-lg mb-lg">404</div>
    <p class="lead m0">Não encontramos nada nesta página.</p>
    <p>A página que você está procurando não existe..</p>
</div>
<div class="input-group mb-xl">
    <input type="text" placeholder="Tente uma pesquisa" class="form-control">
    <span class="input-group-btn">
        <button type="button" class="btn btn-default">
            <em class="fa fa-search"></em>
        </button>
    </span>
</div>
<ul class="list-inline text-center text-sm mb-xl">
    <li><a href="dashboard.html" class="text-muted">Ir para o app</a>
    </li>
    <li class="text-muted">|</li>
    <li><a href="login.html" class="text-muted">Login</a>
    </li>
    <li class="text-muted">|</li>
    <li><a href="register.html" class="text-muted">Registrar</a>
    </li>
</ul>
@endsection